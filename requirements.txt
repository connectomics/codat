# python>=3.6
# tensorflow>=1.8.0 # or gpu version

numpy
scipy
tqdm

# for predict_dense/predict_wkw
wkw>=0.0.5
