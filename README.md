# CODAT

Connectomic data analysis toolkit

## Scope

Neural networks for semantic segmentation tasks in connectomics.

## Packaging

To install the package locally run pip install in the codat main folder

``` bash
pip install .
```

or via a symlink that makes any changes in the repository directly available to
all code on the system via
``` bash
pip install -e .
```

