"""

U-net architecture similar to Funke et al,. 2017
https://arxiv.org/abs/1709.02974
for the SegEM training data with a field of view of ~ 1um^3 (88x88x36 voxels).

@author: @author: Benedikt Staffler <benedikt.staffler@brain.mpg.de

"""


import tensorflow as tf
import codat.nn.unet as net
import codat.data.segem
import codat.training.util
import codat.training.get_empty_gpu
import os
import numpy as np
from codat.training.trainer import train_model_mts as train_model

input_shape = np.asarray([1, 108, 108, 40, 1], dtype=np.uint32)
weight_mem = 12
num_classes = 1
data_format = 'channels_last'
feature_maps = [12, 60, 300, 1500]
# kernel siz contractive path
kernel_size = [[[3, 3, 3], [3, 3, 3]], [[3, 3, 3], [3, 3, 3]],
               [[3, 3, 3], [3, 3, 1]], [[3, 3, 3], [3, 3, 1]]]
# kernel siz expansive path
kernel_size_exp = [[[3, 3, 3], [3, 3, 3]], [[3, 3, 3], [3, 3, 3]],
                   [[3, 3, 3], [3, 3, 3]], [[3, 3, 3], [3, 3, 3]]]
pool_size = None
act_out = None

# training data
TRAIN_DATA_PATH = "/media/benedikt/DATA2/workspace/data/backed/" \
                     "cortexTrainingData/targetKLEE/"

# output paths
output_dir = "/home/benedikt/tmp/unet_1/"
overwrite = False

# training options
max_steps = 1e7
max_runtime = 14 * 24 * 3600  # 14d
loss = 'squared'
add_image_summaries = True
add_weight_summaries = False

optimizer = tf.train.MomentumOptimizer
learning_rate = 1e-5
lr_decay_rate = 0.999  # per 10000 steps
optimizer_args = {'momentum': 0.9}

# logging
LOG_FREQUENCY = 1

# load from checkpoint file
CKPT_FILE = None

# summaries and checkpoints
SAVE_CKPT_SECS = 2 * 3600
SAVE_SUMM_STEPS = 1
LOG_COUNT_STEPS = 1


def get_cnn():
    return net.UNet(feature_maps,
                    num_classes=num_classes,
                    loss=loss,
                    kernel_size_exp=kernel_size_exp,
                    kernel_size=kernel_size,
                    pool_size=pool_size,
                    act_out=act_out)


def train(train_data_path=TRAIN_DATA_PATH,
          ckpt_file=CKPT_FILE):

    # create output dir and copy config
    codat.training.util.create_output_folder(output_dir, overwrite=overwrite)
    codat.training.util.copy_config_to(os.path.realpath(__file__), output_dir)

    # create cnet
    cnet = get_cnn()
    cnet.calculate_border_brute_force(input_shape)

    # training data
    with tf.device('/cpu:0'):
        data = codat.data.segem.default_segem_input_2(train_data_path,
            input_shape[1:-1], input_shape[1:-1] - cnet.border_size,
            weight_mem=weight_mem)

    cnet.build_model(*data)

    # training
    train_model(cnet,
                optimizer=optimizer,
                optimizer_args=optimizer_args,
                learning_rate=learning_rate,
                lr_decay_rate=lr_decay_rate,
                add_weight_summaries=add_weight_summaries,
                add_image_summaries=add_image_summaries,
                log_frequency=LOG_FREQUENCY,
                max_steps=max_steps,
                max_runtime=max_runtime,
                output_dir=output_dir,
                save_ckpt_secs=SAVE_CKPT_SECS,
                save_summ_steps=SAVE_SUMM_STEPS,
                log_count_steps=LOG_COUNT_STEPS,
                ckpt_file=ckpt_file)


if __name__ == "__main__":
    print('Training cnet.')
    codat.training.get_empty_gpu.get_empty_gpu()
    train()