"""
@author: Benedikt Staffler <benedikt.staffler@brain.mpg.de>

Utility functions for datasets.

"""

import h5py


def save_h5(filename, dset_name, data, compression=None):
    """
    Save a single variable to a hdf5 file.
    
    Args:
        filename: Name of the h5 file.
        dset_name: Datasetname in the h5 file.
        data: The actual data.
        compression: string
            'gzip': compatible with matlab
            'lzf', 'zsip'
    """

    h5f = h5py.File(filename, 'w')
    h5f.create_dataset(dset_name, data=data, compression=compression)
    h5f.close()