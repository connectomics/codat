""" Utility functions for tensorflow dataset creation.

@author: Benedikt Staffler <benedikt.staffler@brain.mpg.de

tested with tf1.4

"""

import tensorflow as tf
from codat.data import augmentation
import numpy as np
import scipy.io as sio
import os
import glob
from typing import List, Tuple, Dict


def matfile_loader(file: Tuple[str, str]):
    """
    Function to load data volumes from matfiles.
    Args:
        file:
            Tuple of the form (filepath, raw_var_name (, target_var_name))
            See also FCNData input_files.
    Returns:
        raw: The variable raw_var_name from the file at filepath.
        target: The variable target_var_name if specified from the file at
            filepath.

    """

    # NOTE(amotta): NumPy arrays created with
    #
    #   matfile = sio.loadmat(file[0])
    #   raw = np.asarray(matfile[file[1]])
    #
    # are F_CONTIGUOUS. This means that NumPy or TensorFlow will have to
    # convert the array from Fortran- to C-order at some point.
    #
    # Valentin Pinkau of Scalable Minds has found that at least NumPy's
    # conversion from Fortran- to C-order is surprisingly slow. This could be
    # one of the culprits causing relatively low GPU usage.
    #
    # So, let's just do the "expensive" conversion right here, right now. All
    # subsequent accesses (and there are many more of these than reads of the
    # MAT files) will profit from that.
    aa = lambda x: np.asarray(x, order='C')

    if len(file) == 3:
        print("Loading file {}".format(file[0]))
        matfile = sio.loadmat(file[0])
        raw = aa(matfile[file[1]])
        target = aa(matfile[file[2]])
        print('Input size: {}, target size: {}'.format(raw.shape, target.shape))
    elif len(file) == 4:
        print("Loading input file {}".format(file[0]))
        matfile = sio.loadmat(file[0])
        raw = aa(matfile[file[1]])
        print("Input shape {}".format(raw.shape))

        print("Loading target file {}".format(file[2]))
        matfile = sio.loadmat(file[2])
        target = aa(matfile[file[3]])
        print("Target shape {}".format(target.shape))
    else:
        raise RuntimeError("Unknown file format: {}".format(file))
    return raw, target


class FCNData:
    """
    
    Dataset for 3D voxelwise network training (3D semantic segmentation).
    Input/target volumes are load into RAM (FCNData.cubes_in and
    FCNData.cubes_target) with an associated weight
    (FCNData.cube_sampling_weights) corresponding to the size of the target
    volume. Cubes are then sampled according to their weights and augmented
    using random rotations/flips.
    
    """
    def __init__(self, input_path: str,
                 input_files: List[Tuple[str, str]],
                 target_path: str=None,
                 target_files: List[Tuple[str, str]]=None,
                 target_dtype='float32',
                 cube_loader=matfile_loader,
                 input_shape: List=None,
                 out_shape: List=None,
                 batch_size=1,
                 data_format='channels_last',
                 preproc_input=None,
                 preproc_target=None,
                 rotate=True,
                 flip=True,
                 class_weights: Dict[float, float]=None,
                 add_noise: Dict[str, float]=None,
                 mult_noise: Dict[str, float]=None,
                 cutout=None,
                 mixup=None,
                 warp=None,
                 f_reject_sample=None,
                 num_calls=2,
                 prefetch=5):
        """
        
        Args:
            input_path: string
                Path to folder with training cubes.
            input_files:
                Tuple of the form (filename, var_name), where filename specifies
                the name of a file in the input_path and var_name specifies the
                variable of the input volume in the file.
                Alternatively, the tuple can be of the form
                (filename, in_var_name, target_var_name), where the
                target_var_name is the name of the label volume if it is stored
                in the same file as the input volume.
            target_path: 
                Path to target volume directory (not needed if input files
                contains input and target volumes).
            target_files: 
                Same as input_files but for the target cubes (not needed if
                input files contains input and target volumes).
            target_dtype:
                Data type of the target volumes.
            cube_loader: 
                Function used to load the input/target files.
            input_shape: 
                Spatial shape of the input cubes that the dataset produces.
            out_shape: 
                Spatial shape of the output cubes that the dataset produces.
            batch_size: 
                The batch size for training.
            data_format: 
                'channels_first' or 'channels_last'
            preproc_input: 
                Function that is applied to the raw data after it was cropped to
                input_shape.
            preproc_target:
                Function that is applied to the label data after it was cropped
                to out_shape.
            rotate: 
                Performs random rotations by multiples of 90 degree in the first
                two data dimensions.
            flip:
                Performs random flips in the third data dimension.
            class_weights: 
                Dictionary of the form {class: weight} that assigns the weight
                to the corresponding class label.
            add_noise: dict or None
                Dictionary containing "mean" and "std" and "mean_std" for
                additive Gaussian noise.
            mult_noise: dict or None
                Dictionary containing the "mean" and "std" and "range_mean" for
                multiplicative Gaussian white noise.
            cutout: dict or None
                Dictionary with cutout parameters (mainly the cutout_size) or
                None for no cutout augmentation.
                see codat/data/augmentation.py
            mixup: None or dict
                Parameter for input mixup augmentation.
                'p': float specified the probability of performing mixup.
                'args': dict can specify the names arguments in
                    codat/data/augmentation.py
            warp: None or dict
                Parameter for elastic deformation augmentation.
                'p': probability of applying a elastic deformation
                'args': dictionary with elastic deformation hyperparameters:
                    see augmentation.warp_patch inputs
            f_reject_sample:
                Function of the form toReject = f_reject_sample(input, target)
                that decides whether to reject a current input/target pair after
                cropping, i.e. f_reject_sample should return true if the sample
                was rejected.
            num_calls: 
                Number of parallel calls (see dataset.map) for the python
                preprocessing pipeline.
            prefetch: 
                see dataset.prefetch
        """

        # the actual data cubes
        self.input_path = input_path
        self.input_files = input_files
        self.target_path = target_path
        self.target_files = target_files
        self.target_dtype = target_dtype
        self.cubes_in = []
        self.cubes_target = []
        self.cube_sampling_weights = []
        self.cube_loader = cube_loader
        self.patch_shape = input_shape
        self.out_shape = out_shape
        self.batch_size = batch_size
        self.data_format = data_format
        self.preproc_input = preproc_input
        self.preproc_target = preproc_target
        self.rotate = rotate
        self.flip = flip
        self.class_weights = class_weights
        self.num_calls = num_calls
        self.prefetch = prefetch
        self.f_reject_sample = f_reject_sample
        self.add_noise = {'mean': 0, 'std': 0, 'std_mean': 0}
        if add_noise is not None:
            self.add_noise.update(add_noise)
        self.mult_noise = {'mean': 1, 'std': 0, 'mean_range': 0}
        if mult_noise is not None:
            self.mult_noise.update(mult_noise)
        self.cutout = cutout
        self.mixup = {'p': 0, 'args': {}}
        if mixup is not None:
            self.mixup['p'] = mixup['p']
            if 'args' in mixup:
                self.mixup['args'].update(mixup['args'])
        self.warp = {'p': 0,
                     'args': {'lock_z': True,
                              'amount': 1,
                              'target_discrete_idx': None,
                              'perspective': False,
                              'red_z': 0.4}}
        if warp is not None:
            self.warp['p'] = warp['p']
            if 'args' in warp:
                self.warp['args'].update(warp['args'])
        self.load_cubes()
        self.print_settings()

    def load_cubes(self):
        print("Loading data from {} files:".format(len(self.input_files)))

        def get_files(self):
            if self.target_files is not None:
                for (in_file, in_key), (out_file, out_key) in \
                        zip(self.input_files, self.target_files):
                    yield (os.path.join(self.input_path, in_file), in_key,
                           os.path.join(self.target_path, out_file), out_key)
            else:
                # assume targets are save in input cubes with key specified as
                # third element in input files
                for (in_file, in_key, out_key) in self.input_files:
                    yield (os.path.join(self.input_path, in_file), in_key,
                           out_key)

        weights = []
        for file in get_files(self):
            raw, target = self.cube_loader(file)
            self._validate_border_size(raw, target)
            self.cubes_in.append(raw)
            self.cubes_target.append(target)
            weights.append(target.size)
        if len(self.cubes_in) == 0:
            raise RuntimeError('No input cubes were loaded.')

        weights = np.asarray(weights)
        weights = weights / weights.sum()
        self.cube_sampling_weights = weights
        self._add_channel_dim()
        print("Loading of {} files finished.".format(len(self.input_files)))

    def _validate_border_size(self, raw, target):
        aa = lambda x: np.asarray(x)
        get_shape = lambda x: np.asarray(x.shape[:3])
        b = aa(self.patch_shape) - aa(self.out_shape)
        not_enough_border=False
        raw_shape = get_shape(raw)
        target_shape = get_shape(target)
        b1 = raw_shape - target_shape
        if np.any(b > b1):
            not_enough_border=True
        if not_enough_border:
            print("WARNING: The border around the training cubes is "
                  "too small to allow access to all labels.")
            print("Required border {}. Actual border {}.".format(b, b1))

    def _add_channel_dim(self):

        def add_channels(cube, dim):
            if cube.ndim == 3:
                cube = np.expand_dims(cube, dim)
            return cube

        if self.data_format == 'channels_last':
            dim = 4
        else:
            dim = 1

        fmap = lambda x: add_channels(x, dim)
        self.cubes_in = list(map(fmap, self.cubes_in))
        self.cubes_target = list(map(fmap, self.cubes_target))

    def __iter__(self):
        return self

    def as_generator(self):
        while True:
            yield self.next()

    def next(self):
        """

        Returns:
            next_element: the index of the next cube
        """
        idx = np.random.choice(len(self.cubes_in), p=self.cube_sampling_weights)
        return idx

    def _get_cube(self, idx):
        return self.cubes_in[idx], self.cubes_target[idx]

    def _sample_mixup_cube(self):
        cube_raw, cube_target = self._get_cube(self.next())
        do_sample = True
        while do_sample:
            raw, target = sample_patch(self.patch_shape, self.out_shape,
                                       cube_raw, cube_target, self.data_format)
            if self.f_reject_sample is not None:
                do_sample = self.f_reject_sample(raw, target)
            else:
                do_sample = False

        raw = np.asarray(raw, dtype=np.float32)

        if self.preproc_input is not None:
            raw = self.preproc_input(raw)

        if self.preproc_target is not None:
            target = self.preproc_target(target)

        if self.rotate:
            raw, target, _ = rotate_data(raw, target,
                                         data_format=self.data_format)
        if self.flip:
            raw, target, _ = flip_data(raw, target,
                                       data_format=self.data_format)
        return raw, target

    def _get_training_example(self, idx):
        cube_raw, cube_target = self._get_cube(idx)
        do_sample = True
        while do_sample:
            raw, target = sample_patch(self.patch_shape, self.out_shape,
                                       cube_raw, cube_target,
                                       data_format=self.data_format,
                                       warp=self.warp)
            if self.f_reject_sample is not None:
                do_sample = self.f_reject_sample(raw, target)
            else:
                do_sample = False

        raw = np.asarray(raw, dtype=np.float32)
        if self.preproc_input is not None:
            raw = self.preproc_input(raw)

        if self.preproc_target is not None:
            target = self.preproc_target(target)

        if self.rotate:
            raw, target, _ = rotate_data(raw, target,
                                         data_format=self.data_format)
        if self.flip:
            raw, target, _ = flip_data(raw, target,
                                       data_format=self.data_format)

        mixup_done = False
        if self.mixup['p'] > 0 and (np.random.rand() <= self.mixup['p']):
            mixup_done = True
            raw2, target2 = self._sample_mixup_cube()
            t_copy = target.copy()
            raw, target, l = augmentation.input_mixup(raw, target, raw2,
                                                      target2,
                                                      **self.mixup['args'])

        target = np.asarray(target, dtype=self.target_dtype)

        raw = apply_additive_noise(raw, **self.add_noise)
        raw = apply_mult_noise(raw, **self.mult_noise)

        if self.cutout is not None:
            raw = augmentation.cutout(raw, **self.cutout)

        if self.class_weights is not None:
            weights = np.ones(target.shape, dtype=np.float32)
            for (c, w) in self.class_weights.items():
                if mixup_done:
                    # same convex combination for weights
                    weights[(t_copy == c) & (target2 == c)] = w
                    weights[(t_copy == c) & (target2 != c)] = l * w
                    weights[(t_copy != c) & (target2 == c)] = (1 - l) * w
                else:
                    weights[target == c] = w
            return raw, target, weights
        else:
            return raw, target

    def _set_tensor_shape(self, raw, target, weights=None):
        bs = self.batch_size
        ps = self.patch_shape
        o_shp = self.out_shape

        if self.data_format == 'channels_last':
            c_in = self.cubes_in[0].shape[-1]
            c_out = self.cubes_target[0].shape[-1]
            raw.set_shape(np.concatenate(([bs, ], ps, [c_in, ])))
            target.set_shape(np.concatenate(([bs, ], o_shp, [c_out, ])))
            if weights is not None:
                weights.set_shape(np.concatenate(([bs, ], o_shp, [c_out, ])))
        else:
            c_in = self.cubes_in[0].shape[1]
            c_out = self.cubes_target[0].shape[1]
            raw.set_shape(np.concatenate(([bs, c_in], ps)))
            target.set_shape(np.concatenate(([bs, c_out], o_shp)))
            if weights is not None:
                weights.set_shape(np.concatenate(([bs, c_out], o_shp)))
        if weights is not None:
            return raw, target, weights
        else:
            return raw, target

    def as_tf_dataset(self):
        dset = tf.data.Dataset.from_generator(self.as_generator, (tf.int32))
        mf = lambda i: self._get_training_example(i)
        tf_out_dtype = tf.as_dtype(self.target_dtype)
        if self.class_weights is None:
            outputs = [tf.float32, tf_out_dtype]
        else:
            outputs = [tf.float32, tf_out_dtype, tf.float32]
        dset = dset.map(lambda i: tuple(tf.py_func(mf, [i], outputs)),
                        num_parallel_calls=self.num_calls)
        # NOTE(amotta): The `drop_remainder` flag is set, so that TensorFlow
        # correctly sets the batch size in the output shape. Since we have an
        # "endless" stream of trainig samples, we don't actually drop anything.
        dset = dset.batch(self.batch_size, drop_remainder=True)
        dset = dset.map(lambda *args: tuple(self._set_tensor_shape(*args)),
                        num_parallel_calls=self.num_calls)
        dset = dset.prefetch(self.prefetch)
        return dset

    def get_one_shot_iterator_outputs(self):
        """
        
        Returns:
            data: Tuple
                Tuple consisting of (raw, targets) or (raw, targets, weights)
                if self.class_weights is not none
                The dimensions for each cube is given by
                (batch_size, x, y, z, channels)
        """
        dset = self.as_tf_dataset()
        train_it = dset.make_one_shot_iterator()
        data = train_it.get_next()
        data = self._set_tensor_shape(*data)
        return data

    def print_settings(self):
        print()
        print('Dataset options:')
        if self.class_weights is not None:
            print('Class weights: {}'.format(self.class_weights))
        print('Random rotations by 90 deg: {}'.format(self.rotate))
        print('Random flips along z-direction: {}'.format(self.flip))
        print('Additive noise settings: {}'.format(self.add_noise))
        print('Multiplicative noise settings: {}'.format(self.mult_noise))
        print('Warping settings: {}'.format(self.warp))
        if self.cutout is not None:
            print('Using cutout with settings: {}.'.format(self.cutout))
        if self.f_reject_sample is not None:
            print('Using custom rejection function \'{}\'.'
                  .format(self.f_reject_sample.__name__))
        if self.preproc_input is not None:
            print('Using custom input preprocessing function \'{}\'.'
                  .format(self.preproc_input.__name__))
        if self.preproc_target is not None:
            print('Using custom target preprocessing function \'{}\'.'
                  .format(self.preproc_target.__name__))
        print()


def sample_patch(patch_shape, out_shape, raw, target,
                 data_format='channels_last',
                 warp=None):
    aa = lambda x: np.asarray(x, dtype=np.uint32)
    raw_shape = aa(raw.shape)
    target_shape = aa(target.shape)
    patch_shape = aa(patch_shape)
    out_shape = aa(out_shape)

    assert np.all(patch_shape >= out_shape)
    border_size = (patch_shape - out_shape) // 2 # between input and output
    assert np.all(raw_shape[:-1] >= target_shape[:-1])
    b = (raw_shape[:-1] - target_shape[:-1]) // 2 # actual available border

    # implementation of warping
    if warp is not None and (np.random.rand() <= warp['p']):
        wraw, wtarget, _ = augmentation.warp_patch(patch_shape, raw, target,
                                                   patch_out_shape=out_shape,
                                                   **warp['args'],
                                                   data_format=data_format)
        if wtarget is not None:
            # return result if not failed otherwise do non-warped sampling
            return wraw, wtarget

    # random offset in full target volume
    assert np.all(target_shape[:3] >= out_shape[:3])
    off_to = (target_shape[:3] - out_shape[:3]) + 1

    offset = np.zeros((3,), dtype=np.uint32)
    offset[0] = np.random.randint(0, off_to[0], 1, dtype=offset.dtype)
    offset[1] = np.random.randint(0, off_to[1], 1, dtype=offset.dtype)
    offset[2] = np.random.randint(0, off_to[2], 1, dtype=offset.dtype)

    ts = aa(out_shape)
    input_from = b + offset - border_size
    input_to = b + offset + border_size + ts
    if data_format == 'channels_last':
        raw = raw[input_from[0]:input_to[0],
                  input_from[1]:input_to[1],
                  input_from[2]:input_to[2], :].copy()
        target = target[offset[0]:(offset[0] + ts[0]),
                        offset[1]:(offset[1] + ts[1]),
                        offset[2]:(offset[2] + ts[2]), :].copy()
    else:
        raw = raw[:,
                  input_from[0]:input_to[0],
                  input_from[1]:input_to[1],
                  input_from[2]:input_to[2]].copy()
        target = target[:,
                        offset[0]:(offset[0] + ts[0]),
                        offset[1]:(offset[1] + ts[1]),
                        offset[2]:(offset[2] + ts[2])].copy()
    return raw, target


def rotate_data(raw, target, weights=None, data_format='channels_last'):
    k = np.random.randint(0, 5, 1)
    if data_format == 'channels_last':
        axes = (0, 1)
    else:
        axes = (1, 2)
    raw = np.rot90(raw, k, axes)
    target = np.rot90(target, k, axes)
    if weights is not None:
        weights = np.rot90(weights, k, axes)
    return raw, target, weights


def flip_data(raw, target, weights=None, data_format='channels_last'):
    if np.random.rand(1) > 0.5:
        if data_format == 'channels_last':
            dim = 2
        else:
            dim = 3
        raw = np.flip(raw, dim)
        target = np.flip(target, dim)
        if weights is not None:
            target = np.flip(weights, dim)
    return raw, target, weights


def apply_additive_noise(raw, mean=0, std=0, std_mean=0):
    if std_mean > 0:
        mean = mean + np.random.randn(1) * std_mean
    if std == 0:
        raw = raw + mean
    elif std > 0:
        raw = raw + (mean + np.random.randn(*raw.shape) * std)
    return raw.astype(np.float32)


def apply_mult_noise(raw, mean=1, std=0, mean_range=0):
    if mean_range > 0:
        mean = mean + np.random.uniform(-mean_range / 2., mean_range / 2., 1)
    if std == 0:
        raw = raw * mean
    elif std > 0:
        raw = raw * (mean + np.random.randn(*raw.shape) * std)
    return raw.astype(np.float32)


def from_file_list(files, map_func, Tout, num_parallel_call=None, weights=None):
    """
    Create a dataset that loads  the specified files via a custom
    python function.
    Args:
        files: list or string
            List of files to load or string for from_files dataset
        map_func: function
            Mapping function that is used to load each file specified in files.
        Tout: list
            List of output tensor dtype
        num_parallel_call: int
            Number of parallel calls in dataset preprocessing.
        weights: list
            weights for each file in files
            (Default: 1)
    Returns:
        dataset: tf.data.Dataset object

    """

    if weights is None:
        if type(files) == list:
            files_t = tf.constant(files)
            dataset = tf.data.Dataset.from_tensor_slices(files_t)
        else:
            dataset = tf.data.Dataset.list_files(files)
    else:
        data_provider = FileLoader(files, weights)
        dataset = data_provider.as_tf_dataset()
    dataset = dataset.map(lambda filename: tuple(tf.py_func(map_func,
                                                            [filename], Tout)),
                          num_parallel_calls=num_parallel_call)
    return dataset


class FileLoader:
    def __init__(self, files, weights=None):
        if type(files) == list:
            self.files = files
        else:
            self.files = glob.glob(files)
        if weights is not None:
            weights = np.asarray(weights, np.float32)
            weights = weights / weights.sum()
            self.weights = weights
            assert self.weights.size == len(self.files), "The weights must be" \
                " specified for each file"
        else:
            self.weights = np.ones(len(self.files), dtype=np.float32) /\
                           len(self.files)

    def __iter__(self):
        return self

    def as_generator(self):
        while True:
            yield self.next()

    def next(self):
        """
        
        Returns:
            next_element: the next filename as a bytestring
        """
        idx = np.random.choice(len(self.files), p=self.weights)
        return self.files[idx].encode()

    def as_tf_dataset(self):
        return tf.data.Dataset.from_generator(self.as_generator, (tf.string))

