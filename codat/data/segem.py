""" Segem training data loader

@author: Benedikt Staffler <benedikt.staffler@brain.mpg.de

tested with tf1.4

"""

import numpy as np
import scipy.ndimage.morphology as morph
import glob
import scipy.io as sio
import os
import tensorflow as tf
import codat.data.dataset
from typing import List


def default_segem_input_2(folder, input_shape, output_shape,
                          weight_mem=12, weight_glia=0,
                          batch_size=1, num_calls=2,
                          data_format='channels_last',
                          add_noise=None,
                          mult_noise=None,
                          cutout=None,
                          mixup=None,
                          warp=None,
                          dset=[]):
    files = glob.glob(os.path.join(folder, '*.mat'))
    files = map(os.path.basename, files)
    files = [(file, 'raw', 'target') for file in files]
    class_weights = {-1: weight_mem, 0: weight_glia}
    dataset = codat.data.dataset.FCNData(input_path=folder,
                                         input_files=files,
                                         input_shape=input_shape,
                                         out_shape=output_shape,
                                         preproc_input=preproc_raw,
                                         class_weights=class_weights,
                                         batch_size=batch_size,
                                         num_calls=num_calls,
                                         data_format=data_format,
                                         add_noise=add_noise,
                                         mult_noise=mult_noise,
                                         cutout=cutout,
                                         mixup=mixup,
                                         warp=warp)
    data = dataset.get_one_shot_iterator_outputs()
    dset.append(dataset)
    return data


def default_segem_input_ce_2(folder, input_shape, output_shape,
                             batch_size=1, num_calls=2,
                             data_format='channels_last',
                             add_noise=None,
                             mult_noise=None):
    def preproc_target(x):
        # make integer labels
        out = np.zeros(x.shape, dtype=np.int32)
        out[x == -1] = 1
        out[x == 1] = 0
        return out

    files = glob.glob(os.path.join(folder, '*.mat'))
    files = map(os.path.basename, files)
    files = [(file, 'raw', 'target') for file in files]
    dataset = codat.data.dataset.FCNData(input_path=folder,
                                         input_files=files,
                                         target_dtype='int32',
                                         input_shape=input_shape,
                                         out_shape=output_shape,
                                         preproc_input=preproc_raw,
                                         preproc_target=preproc_target,
                                         batch_size=batch_size,
                                         num_calls=num_calls,
                                         data_format=data_format,
                                         add_noise=add_noise,
                                         mult_noise=mult_noise
                                         )
    data = dataset.get_one_shot_iterator_outputs()
    return data


def preproc_raw(raw):
    return (np.asarray(raw, dtype=np.float32) - 122.) / 22.


def _read_cube(filename: str,
               input_name: str = 'raw',
               label_name: str = 'target'):
    """
    Load a SegEM training cube.
    Args:
        filename: string
            Full path to training cube.
        input_name: Name of the input variable.
        label_name: Name of the label variable.

    Returns:
        raw: np.uint8 array
        target: np.float32 array
    """
    mat_file = sio.loadmat(filename)
    raw = np.asarray(mat_file[input_name])
    labels = np.asarray(mat_file[label_name], dtype=np.float32)
    return raw, labels


def _read_cube_py(filename: str,
                  border_size: List[int],
                  target_size: List[int],
                  target_mode='tanh',
                  weight_mem=1,
                  data_format='channels_last',
                  rot=True,
                  mirror=True,
                  input_name: str = 'raw',
                  label_name: str = 'target',
                  normalize_raw=True,
                  label_offset: float = 0,
                  label_smoothing=(0, True)):
    """
    Python function to read an SegEM training cube. Cubes are tiled into patches
    of target_size which are sampled uniformly. If target_size does not exactly
    tile the target cube then then the parts that are not tiled are discarded
    (tiling is starting from [0, 0, 0] with steps of target_size).
    Args:
        filename: A bytestring with the filename to read from.
        target_size: Target size of the form (x, y, z)
        target_mode: 'tanh' for outputs in [-1, 1] (-1 membrane)
                     'prob' for outputs in [0, 1] (1 membrane)
        weight_mem: weight of membrane class relative to non-membrane
        data_format: 'channels_last' adds an additional dimension at the end
                    'channels_first' add an additional dimension at the
                        beginning
        rot: rotate by random multiple of 90° in x-y plane
        mirror: random mirroring along z-axis
        input_name: Name of the input variable.
        label_name: Name of the label variable.
        normalize_raw: logical or function handle
            logical: if true then raw data normalization via (x - 122.) / 22.
            function: custom raw data normalization
        label_offset: Only for target_mode prob. Add/subtract a small
            offset from the maximal label value.
        label_smoothing: tuple of parameters for label smoothing
    Returns:
        raw: float32 array of size TARGET_SIZE + BORDER_SIZE containing an input
            training cube.
        labels: float32 array of size TARGET_SIZE containing the labels for the
            training cube.
        weights: float32 array of same size as labels that contains the weights
            for each voxel to discard. All weights sum to weights.size
            (normalization for tf.losses.mean_squared_error)
    """

    raw, labels = _read_cube(filename, input_name=input_name,
                             label_name=label_name)

    if label_smoothing[0] > 0:
        alpha = label_smoothing[0]
        if label_smoothing[1]:
            d = np.zeros(labels.shape, dtype=np.float32)
            for i in range(labels.shape[2]):
                d[:, :, i] = morph.distance_transform_edt(labels[:, :, i] > -1)
        else:
            d = morph.distance_transform_edt(labels > -1)
        d = 4. / (1 + np.exp(- alpha * d ** 2)) - 3
        d[d > 1] = 1
        mask = labels != -1
        labels[mask] = d[mask]

    if raw.ndim == 3:
        raw = np.expand_dims(raw, 4)
    raw_shape = np.asarray(raw.shape)
    lab_shape = np.asarray(labels.shape)
    border_size = np.asarray(border_size)
    target_size = np.asarray(target_size)
    offset = np.zeros((3, ), dtype=np.uint32)
    max_offsets = np.floor((lab_shape - target_size) / target_size)
    offset[0] = np.random.randint(0, max_offsets[0] + 1, 1) * target_size[0]
    offset[1] = np.random.randint(0, max_offsets[1] + 1, 1) * target_size[1]
    offset[2] = np.random.randint(0, max_offsets[2] + 1, 1) * target_size[2]
    b = (raw_shape[:-1] - lab_shape) / 2
    ts = np.asarray(target_size, dtype=np.uint32)
    input_from = b + offset - border_size / 2
    input_to = b + offset + border_size / 2 + ts
    input_from = np.asarray(input_from, dtype=np.uint32)
    input_to = np.asarray(input_to, dtype=np.uint32)
    raw = raw[input_from[0]: input_to[0],
              input_from[1]: input_to[1],
              input_from[2]: input_to[2], :]
    if type(normalize_raw) is bool:
        raw = np.asarray(raw, dtype=np.float32)
        if normalize_raw:
            raw = (np.asarray(raw, dtype=np.float32) - 122.) / 22.
    else:
        raw = normalize_raw(raw)

    labels = labels[offset[0]: offset[0] + ts[0],
                    offset[1]: offset[1] + ts[1],
                    offset[2]: offset[2] + ts[2]]

    labels = np.asarray(labels, dtype=np.float32)

    # random rotation in xy
    if rot:
        k = np.random.randint(0, 5, 1)
        raw = np.rot90(raw, k)
        labels = np.rot90(labels, k)

    # random flips in z
    if mirror and np.random.rand(1) > 0.5:
        raw = np.flip(raw, 2)
        labels = np.flip(labels, 2)

    weights = np.asarray(labels != 0, dtype=np.float32)
    weights[labels == -1] = weight_mem
    weights = weights * weights.size / weights.sum()

    if target_mode == 'prob':
        labels[labels == 1] = 0 + label_offset
        labels[labels == -1] = 1 - label_offset
        labels = np.asarray(labels, np.int32)  # for sparse tf cross-entropy

    if data_format == 'channels_last':
        labels = np.expand_dims(labels, 4)
        weights = np.expand_dims(weights, 4)
    elif data_format == 'channels_first':
        raw = np.transpose(raw, (3, 0, 1, 2))
        labels = np.expand_dims(labels, 0)
        weights = np.expand_dims(weights, 0)
    return raw, labels, weights


def file_dataset(folder, border_size, output_size, num_parallel_calls=None):
    filenames = glob.glob(os.path.join(folder, '*.mat'))
    if not filenames:
        RuntimeError("Filenames empty.")
    filenames_t = tf.constant(filenames)
    dataset = tf.data.Dataset.from_tensor_slices((filenames_t))
    border_size = np.asarray(border_size, dtype=np.uint32)
    output_size = np.asarray(output_size, dtype=np.uint32)
    if output_size.size > 3:
        output_size = output_size[1:4]
    parseFile = lambda filename: _read_cube_py(filename, border_size,
                                               output_size)[0:2]
    dataset = dataset.map(lambda filename: tuple(tf.py_func(parseFile,
                                                            [filename],
                                                            [tf.float32,
                                                             tf.float32])),
                          num_parallel_calls=num_parallel_calls)
    dataset = dataset.repeat()
    return dataset


def default_segem_input(folder: str,
                        border_shape: List[int],
                        output_shape=(25, 25, 25),
                        weight_mem=6,
                        num_parallel_calls=2,
                        label_smoothing=(0, True),
                        data_format='channels_last'):
    """
    Default data loading for squared error training.

    Args:
        folder: Path to SegEM matfiles.
        border_shape: [3x1] array-like with the border shape around the target
            in each spatial dimension
        output_shape: [3x1] array-like with target shape in each spatial
            dimension
        weight_mem: weight of membrane labels relative to non-membrane labels
        num_parallel_calls: parallel calls in dataset.map
        label_smoothing: Target value smoothing.
        data_format: 'channels_first' or 'channels_last'
    Returns:
        raw: input tensor
        labels: label tensor which is smaller by border shape then input tensor
        weights: weight tensor of same size as label tensor
    """

    border_shape = np.asarray(border_shape, dtype=np.uint32)
    output_shape = np.asarray(output_shape, dtype=np.uint32)

    filenames = glob.glob(os.path.join(folder, '*.mat'))
    if not filenames:
        RuntimeError("No training cubes were found.")
    filenames_t = tf.constant(filenames)
    parseFile = lambda filename: _read_cube_py(filename, border_shape,
                                               output_shape,
                                               weight_mem=weight_mem,
                                               data_format=data_format,
                                               label_smoothing=label_smoothing)
    dataset = tf.data.Dataset.from_tensor_slices((filenames_t))
    dataset = dataset.map(lambda filename: tuple(tf.py_func(parseFile,
                                                            [filename],
                                                            [tf.float32,
                                                             tf.float32,
                                                             tf.float32])),
                          num_parallel_calls=num_parallel_calls)
    dataset = dataset.repeat()
    dataset = dataset.batch(1)
    # dataset = dataset.prefetch(10)
    train_it = dataset.make_one_shot_iterator()
    raw, labels, weights = train_it.get_next()
    if data_format == 'channels_last':
        raw.set_shape(
            np.concatenate(([1, ], border_shape + output_shape, [1, ])))
        labels.set_shape(np.concatenate(([1, ], output_shape, [1, ])))
        weights.set_shape(np.concatenate(([1, ], output_shape, [1, ])))
    else:
        raw.set_shape(
            np.concatenate(([1, 1], border_shape + output_shape)))
        labels.set_shape(np.concatenate(([1, 1], output_shape)))
        weights.set_shape(np.concatenate(([1, 1], output_shape)))
    return raw, labels, weights


def segem_recursive_input(folder: str,
                          border_shape: List[int],
                          output_shape=(25, 25, 25),
                          weight_mem=6,
                          num_parallel_calls=2,
                          data_format='channels_last'):
    """
        Default data loading for squared error training.

        Args:
            folder: Path to SegEM matfiles.
            border_shape: [3x1] array-like with the border shape around the target
                in each spatial dimension
            output_shape: [3x1] array-like with target shape in each spatial
                dimension
            weight_mem: weight of membrane labels relative to non-membrane labels
            num_parallel_calls: parallel calls in dataset.map
            data_format: 'channels_first' or 'channels_last'
        Returns:
            raw: input tensor
            labels: label tensor which is smaller by border shape then input tensor
            weights: weight tensor of same size as label tensor
        """

    border_shape = np.asarray(border_shape, dtype=np.uint32)
    output_shape = np.asarray(output_shape, dtype=np.uint32)

    filenames = glob.glob(os.path.join(folder, '*.mat'))
    if not filenames:
        RuntimeError("No training cubes were found.")
    filenames_t = tf.constant(filenames)
    parseFile = lambda filename: _read_cube_py(filename, border_shape,
                                               output_shape,
                                               weight_mem=weight_mem,
                                               data_format=data_format,
                                               normalize_raw=False)
    dataset = tf.data.Dataset.from_tensor_slices((filenames_t))
    dataset = dataset.map(lambda filename: tuple(tf.py_func(parseFile,
                                                            [filename],
                                                            [tf.float32,
                                                             tf.float32,
                                                             tf.float32])),
                          num_parallel_calls=num_parallel_calls)
    dataset = dataset.repeat()
    dataset = dataset.batch(1)
    # dataset = dataset.prefetch(10)
    train_it = dataset.make_one_shot_iterator()
    raw, labels, weights = train_it.get_next()
    if data_format == 'channels_last':
        raw.set_shape(
            np.concatenate(([1, ], border_shape + output_shape, [2, ])))
        labels.set_shape(np.concatenate(([1, ], output_shape, [1, ])))
        weights.set_shape(np.concatenate(([1, ], output_shape, [1, ])))
    else:
        raw.set_shape(
            np.concatenate(([1, 2], border_shape + output_shape)))
        labels.set_shape(np.concatenate(([1, 1], output_shape)))
        weights.set_shape(np.concatenate(([1, 1], output_shape)))
    return raw, labels, weights


def segem_ce_input(folder: str,
                   border_shape: List[int],
                   output_shape=(25, 25, 25),
                   num_parallel_calls=2):
    """
    Default data loading for training with 2-class cross-entropy.

    Args:
        folder: Path to SegEM matfiles.
        border_shape: [3x1] array-like with the border shape around the target
            in each spatial dimension
        output_shape: [3x1] array-like with target shape in each spatial
            dimension
        num_parallel_calls: parallel calls in dataset.map

    Returns:
        raw: input tensor
        labels: label tensor which is smaller by border shape then input tensor
    """

    border_shape = np.asarray(border_shape, dtype=np.uint32)
    output_shape = np.asarray(output_shape, dtype=np.uint32)

    filenames = glob.glob(os.path.join(folder, '*.mat'))
    if not filenames:
        RuntimeError("No training cubes were found.")
    filenames_t = tf.constant(filenames)
    parseFile = lambda filename: _read_cube_py(filename, border_shape,
                                               output_shape,
                                               target_mode="prob")[0:2]
    dataset = tf.data.Dataset.from_tensor_slices((filenames_t))
    dataset = dataset.map(lambda filename: tuple(tf.py_func(parseFile,
                                                            [filename],
                                                            [tf.float32,
                                                             tf.int32])),
                          num_parallel_calls=num_parallel_calls)
    dataset = dataset.repeat()
    dataset = dataset.batch(1)
    # dataset = dataset.prefetch(10)
    train_it = dataset.make_one_shot_iterator()
    raw, labels = train_it.get_next()
    raw.set_shape(np.concatenate(([1, ], border_shape + output_shape, [1, ])))
    labels.set_shape(np.concatenate(([1, ], output_shape, [1, ])))
    return raw, labels


def calculate_recursive_input(cnet,
                              filenames,
                              output_folder: str,
                              input_size: List[int] =None,
                              ckpt_file: str=None):
    """
    Calculate input for recursive training
    Args:
        cnet: A codat.nn object
        filenames: List of filenames or path to input mafiles.
        output_folder: Path to output matfiles.
        input_size: see cnet.predict_dense()
        ckpt_file: Path to checkpoint file that is loaded.
    """

    if filenames is str:
        filenames = sorted(glob.glob(os.path.join(filenames, '*.mat')))
    elif type(filenames) is not list:
        raise RuntimeError("Wrong input format for filenames.")

    for file in filenames:
        raw, labels = _read_cube(file)
        raw = (np.asarray(raw, dtype=np.float32) - 122.) / 22.
        pred = cnet.predict_dense(raw, ckpt_file=ckpt_file,
                                  input_size=input_size)
        pred = pred[0, :, :, :, 0]
        b = (np.asarray(raw.shape) - np.asarray(pred.shape)) // 2
        raw = raw[b[0]:-b[0], b[1]:-b[1], b[2]:-b[2]]
        inputs = np.stack((raw, pred), axis=3)
        b = (np.asarray(inputs.shape[:-1]) - np.asarray(labels.shape)) // 2
        if np.any(b < 0):
            raise RuntimeError("Labels are larger than the recursive input.")
        base_name = os.path.basename(file)
        output_file = os.path.join(output_folder, base_name)
        print('Saving output file {}'.format(output_file))
        sio.savemat(output_file, {'raw': inputs, 'target': labels})


class SegEMDataLoader:
    def __init__(self, train_folder, border_size, target_shape):
        self.train_folder = train_folder
        self.file_names = glob.glob(os.path.join(train_folder, '*.mat'))
        self.border_size = np.asarray(border_size, np.uint32)
        self.target_shape = np.asarray(target_shape, np.uint32)
        if self.target_shape.size == 3:
            self.target_shape = np.concatenate(((1, ), self.target_shape,
                                                (1, )))

    def __iter__(self):
        return self

    def next(self):
        # load a random cube
        cube_no = np.random.randint(0, len(self.file_names), 1, dtype=np.uint32)
        cube_no = np.asscalar(cube_no)
        raw, labels, _ = _read_cube_py(self.file_names[cube_no],
                                       self.border_size, self.target_shape[1:4])
        return raw, labels

    def as_generator(self):
        while True:
            yield self.next()

    def as_tf_dataset(self):

        # shape for raw data volume
        raw_shape = self.target_shape[1:4] + self.border_size
        raw_shape = np.concatenate((raw_shape, (1, )), axis=0)
        print("raw shapes: {}".format(raw_shape))
        print("target shapes: {}".format(self.target_shape))
        shapes = tuple(map(tf.TensorShape, (raw_shape, self.target_shape[1:5])))

        return tf.data.Dataset.from_generator(self.as_generator,
                        (tf.float32, tf.float32),
                        output_shapes=shapes)
