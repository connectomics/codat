""" Data loading utility functons.

@author: Benedikt Staffler <benedikt.staffler@brain.mpg.de

"""

import os
import h5py
import scipy.io as sio
import numpy as np
from typing import List


def read_cube_h5(filename: str,
                 input_name: str = 'raw',
                 label_name: str = 'labels'):
    h5f = h5py.File(filename, 'r')
    raw = h5f[input_name][:]
    labels = h5f[label_name][:]
    return raw, labels


def read_cube_mat(filename: str,
                  input_name: str = 'raw',
                  label_name: str = 'labels'):
    matfile = sio.loadmat(filename)
    raw = np.asarray(matfile[input_name])
    labels = np.asarray(matfile[label_name])
    return raw, labels


def read_cube_py(filename: str,
                 border_size: List[int],
                 target_size: List[int],
                 data_format='channels_last',
                 rot=True,
                 mirror=True,
                 input_name: str = 'raw',
                 label_name: str = 'labels',
                 normalize_raw=True):
    """
    Loader for voxel-wise classification data.
    Args:
        filename: Name of the file (must be .h5 or .mat)
        border_size: Border size in the spatial dimensions
        target_size: Target size in the spatial dimensions
        data_format: 'channels_last' or 'channels_first'
        rot: Random rotation by multiples of 90° in x-y plane
        mirror: Random mirroring in 3rd dimension
        input_name: Variable name of the input in input file
        label_name: Variable name of the labels in input file
        normalize_raw: 
            If bool then normalization is done via (raw - 122.) / 22.
            Can be a function that is applied to raw for custom normalization

    Returns:
        raw: 4d raw cube
        labels: 4d label cube where labels are encoded as integer in spatial
            dimensions
        weights: currently only placeholder
    """
    _, ext = os.path.splitext(filename)
    if isinstance(ext, (bytes, bytearray)):
        ext = ext.decode()
    if ext == '.h5':
        raw, labels = read_cube_h5(filename, input_name, label_name)
    elif ext == '.mat':
        raw, labels = read_cube_mat(filename, input_name, label_name)
    else:
        raise RuntimeError('Unsupported file type {}'.format(ext))

    labels = np.asarray(labels, dtype=np.int32)

    if raw.ndim == 3:
        raw = np.expand_dims(raw, 4)
    raw_shape = np.asarray(raw.shape)
    lab_shape = np.asarray(labels.shape)
    border_size = np.asarray(border_size)
    target_size = np.asarray(target_size)
    offset = np.zeros((3,), dtype=np.uint32)
    max_offsets = np.floor((lab_shape - target_size) / target_size)
    offset[0] = np.random.randint(0, max_offsets[0] + 1, 1) * target_size[0]
    offset[1] = np.random.randint(0, max_offsets[1] + 1, 1) * target_size[1]
    offset[2] = np.random.randint(0, max_offsets[2] + 1, 1) * target_size[2]
    b = (raw_shape[:-1] - lab_shape) / 2
    ts = np.asarray(target_size, dtype=np.uint32)
    input_from = b + offset - border_size / 2
    input_to = b + offset + border_size / 2 + ts
    input_from = np.asarray(input_from, dtype=np.uint32)
    input_to = np.asarray(input_to, dtype=np.uint32)
    raw = raw[input_from[0]: input_to[0],
              input_from[1]: input_to[1],
              input_from[2]: input_to[2], :]
    if type(normalize_raw) is bool:  # always to float32
        raw = np.asarray(raw, dtype=np.float32)
        if normalize_raw:
            raw = (np.asarray(raw, dtype=np.float32) - 122.) / 22.
    else:
        raw = normalize_raw(raw)
    labels = labels[offset[0]: offset[0] + ts[0],
                    offset[1]: offset[1] + ts[1],
                    offset[2]: offset[2] + ts[2]]

    # random rotation in xy
    if rot:
        k = np.random.randint(0, 5, 1)
        raw = np.rot90(raw, k)
        labels = np.rot90(labels, k)

    # random flips in z
    if mirror and np.random.rand(1) > 0.5:
        raw = np.flip(raw, 2)
        labels = np.flip(labels, 2)

    if data_format == 'channels_last':
        labels = np.expand_dims(labels, 4)
    elif data_format == 'channels_first':
        raw = np.transpose(raw, (3, 0, 1, 2))
        labels = np.expand_dims(labels, 0)
    weights = None  # for a possible extension later
    return raw, labels, weights
