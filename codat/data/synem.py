""" SynEM svm training data loader

@author: Benedikt Staffler <benedikt.staffler@brain.mpg.de

"""

import os
import numpy as np
import codat.data.dataset
import codat.data.loader
import glob
import tensorflow as tf
from typing import List


def preproc_raw(raw):
    return (np.asarray(raw, dtype=np.float32) - 122.) / 22.


def default_svm_input_2(folder: str,
                        input_shape: List[int],
                        output_shape: List[int],
                        batch_size=1,
                        num_calls=2,
                        data_format='channels_last',
                        add_noise=None,
                        mult_noise=None):
    files = glob.glob(os.path.join(folder, '*.mat'))
    files = map(os.path.basename, files)
    files = [(file, 'raw', 'seg') for file in files]
    dataset = codat.data.dataset.FCNData(input_path=folder,
                                         input_files=files,
                                         target_dtype='int32',
                                         input_shape=input_shape,
                                         out_shape=output_shape,
                                         preproc_input=preproc_raw,
                                         batch_size=batch_size,
                                         num_calls=num_calls,
                                         data_format=data_format,
                                         add_noise=add_noise,
                                         mult_noise=mult_noise)
    data = dataset.get_one_shot_iterator_outputs()
    return data


def default_svm_input(folder,
                      border_shape: List[int],
                      output_shape: List[int],
                      num_parallel_calls=2,
                      data_format='channels_last',
                      normalize_raw=True):
    """
    Default loader for SVM training data (cross-entropy error with 4 channels).
    Args:
        folder: Path to SynEM SVM mat-files.
        border_shape: [3x1] int array-like of the total border in each spatial
            dimension
        output_shape: [3x1] int array-like of the network output size in each
            spatial dimension
        num_parallel_calls: parallel processor for data preprocessing
        data_format: 'channels_first' or 'channels_last'

    Returns:
        raw: input tensor for the network
        labels: label tensor for training
    """

    border_shape = np.asarray(border_shape, dtype=np.uint32)
    output_shape = np.asarray(output_shape, dtype=np.uint32)

    filenames = sorted(glob.glob(os.path.join(folder, '*.mat')))
    print(filenames)
    if not filenames:
        RuntimeError("No training cubes were found.")
    # weights = [0.0819, 0.0819, 0.0819, 0.0819, 0.0819, 0.0819, 0.5088]
    # file_loader = FileLoader(filenames, weights=weights)
    # dset = file_loader.as_tf_dataset()
    filenames_t = tf.constant(filenames)
    dset = tf.data.Dataset.from_tensor_slices((filenames_t))

    def parse_file(filename):
        raw, labels, _ = codat.data.loader.read_cube_py(filename, border_shape,
            output_shape, data_format, label_name='seg',
            normalize_raw= normalize_raw)
        return raw, labels

    dset = dset.map(lambda filename: tuple(tf.py_func(parse_file, [filename],
                                                      [tf.float32, tf.int32],
                                                      )),
                    num_parallel_calls=num_parallel_calls)
    dset = dset.repeat()
    dset = dset.batch(1)
    # dset = dset.prefetch(100)
    train_it = dset.make_one_shot_iterator()
    raw, labels = train_it.get_next()
    if data_format == 'channels_last':
        raw.set_shape(
            np.concatenate(([1, ], border_shape + output_shape, [1, ])))
        labels.set_shape(np.concatenate(([1, ], output_shape, [1, ])))
    else:
        raw.set_shape(
            np.concatenate(([1, 1], border_shape + output_shape)))
        labels.set_shape(np.concatenate(([1, 1], output_shape)))
    return raw, labels


def rgb_summaries(fcn):
    """
    Simple custom summary function for SynEM SVM data that displays the three
    output classes in an RGB image.
    Args:
        fcn: A codat.nn.fcn object.
    """

    output_shape = fcn.output_tensor.get_shape().as_list()
    input_shape = fcn.input_tensor.get_shape().as_list()

    with tf.name_scope('image_summaries'):
        outs = output_shape
        ins = input_shape

        # output example
        if fcn.data_format == 'channels_last':
            im_out = tf.slice(fcn.output_tensor, [0, 0, 0, 0, 1],
                              [1, outs[1], outs[2], 1, 3])
        else:
            im_out = tf.slice(tf.transpose(fcn.output_tensor,
                                           perm=[0, 2, 3, 4, 1]),
                              [0, 0, 0, 0, 1],
                              [1, outs[1], outs[2], 1, 3])
        im_out = tf.squeeze(im_out, 3)
        if fcn.data_format == 'channels_last':
            label_out = tf.slice(fcn.label_tensor, [0, 0, 0, 0, 0],
                                 [1, outs[1], outs[2], 1, 1])
        else:
            label_out = tf.slice(tf.transpose(fcn.label_tensor,
                                              perm=[0, 2, 3, 4, 1]),
                                 [0, 0, 0, 0, 0],
                                 [1, outs[1], outs[2], 1, 1])
        label_out = tf.squeeze(label_out, 4)
        label_out = tf.squeeze(label_out, 3)
        mask = []
        for i in range(1, 4):
            mask.append(tf.cast(tf.equal(label_out, i),
                                dtype=tf.float32))
        label_out = tf.stack(mask, axis=3)
        im = tf.concat([im_out, label_out], 2)
        tf.summary.image('prediction/', im)

        im_in = tf.slice(fcn.input_tensor,
                         np.asarray([0, 0, 0, fcn.border_size[2] / 2, 0],
                                    dtype=np.int32),
                         [1, ins[1], ins[2], 1, 1])
        im_in = tf.squeeze(im_in, 4)
        tf.summary.image('input/', im_in)