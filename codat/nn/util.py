""" Neural network utility functions.

@author: Benedikt Staffler <benedikt.staffler@brain.mpg.de

tested with tf1.4

"""

import tensorflow as tf
import numpy as np
from typing import List


def create_pl_tensor(inputs: tf.Tensor,
                     dtype=tf.float32,
                     data_format='channels_last'):
    """
    Create an input placeholder for a 3d convnet.
    Args:
        inputs: tuple or list or tensor
            tuple of the input of maximal length 5 with dimensions
            (batch_size, x, y, z, channels). If inputs is a tuple of length
            3 then it is assumed that these are the spatial dimension (x, y, z)
            and the other dimensions are 1.
            If length is 4 then it corresponds to the first 4 dimensions of the
            input tensor (batch_size, x, y, z).
            If the input is a tensor already than nothing is done.
        dtype: datatype of placeholder
        data_format:
            'channels_first' or 'channels_last'
    Returns:
        inputs: tf.tensor
            Placeholder tensor for inputs with shape input_size.
    """
    if type(inputs) == list or type(inputs) == np.ndarray:
        inputs = tuple(inputs)

    if type(inputs) == tuple:
        if len(inputs) == 3:
            if data_format == 'channels_last':
                inputs = (1,) + inputs + (1,)
            elif data_format == 'channels_first':
                inputs = (1, 1, ) + inputs
        elif len(inputs) == 4:
            if data_format == 'channels_last':
                inputs = inputs + (1,)
            elif data_format == 'channels_first':
                inputs = (inputs[0], 1, ) + inputs[1:]
        inputs = tf.placeholder(dtype, inputs)
    return inputs


def tanh_scaled(x: tf.Tensor):
    """
    Scaled tanh function as proposed in LeCun et al., 1998.
    Args:
        x: Input tensor that is passed through the non-linearity.

    Returns:
        y: Output tensor.
    """

    a = tf.constant(0.66)
    b = tf.constant(1.7159)
    return tf.scalar_mul(b, tf.tanh(tf.scalar_mul(a, x)))


def crop(t: tf.Tensor,
         out_shape: List[int],
         data_format='channels_last') -> tf.Tensor:
    ts = t.get_shape().as_list()
    if data_format == 'channels_last':
        begin = [x - y for x, y in zip(ts[1:-1], out_shape[1:-1])]
    else:
        begin = [x - y for x, y in zip(ts[2:], out_shape[2:])]
    assert all(map(lambda x: x % 2 == 0, begin))
    begin = list(map(lambda x: x // 2, begin))  # crop symmetrically
    if data_format == 'channels_last':
        begin = [0] + begin + [0]
        t = tf.slice(t, begin, out_shape)
    else:
        begin = [0, 0] + begin
        t = tf.slice(t, begin, out_shape)
    return t


def concat_channels(t: List[tf.Tensor],
                    data_format='channels_last') -> tf.Tensor:
    if data_format == 'channels_last':
        out = tf.concat(t, axis=4)
    else:
        out = tf.concat(t, axis=1)
    return out


def crop_to_smallest(t: List[tf.Tensor],
                     crop_channels=False,
                     data_format='channels_last') -> List[tf.Tensor]:
    """
    Crop all tensors in a list to the smallest shape occurring in each
    dimension.
    Args:
        t: List of tensor to crop.
        crop_channels: Whether to crop channels as well.
        data_format:

    Returns:
        t: List of tensor cropped to common shape.
    """

    shapes = [x.get_shape().as_list() for x in t]
    sh_smallest = np.asarray(shapes, dtype=np.uint32).min(axis=0)
    for i in range(len(t)):
        if not crop_channels:
            if data_format == 'channels_last':
                sh_smallest[4] = shapes[i][4]
            else:
                sh_smallest[1] = shapes[i][1]
        t[i] = crop(t[i], sh_smallest)
    return t


def cube_to_input_shape(inputs):
    if inputs.ndim == 3:  # 3d input cube (note it will also go in == 4 if)
        inputs = np.expand_dims(inputs, 3)
    if inputs.ndim == 4:  # 4d input cube
        inputs = np.expand_dims(inputs, 0)
    return inputs
