""" Fully convolutional neural network base class.

@author: Benedikt Staffler <benedikt.staffler@brain.mpg.de

tested with tf1.4

"""

import tensorflow as tf
import numpy as np
from typing import List
from codat.nn.util import cube_to_input_shape, create_pl_tensor
import codat.training.losses
import os
import glob


def _get_shp(x):
    return x.get_shape().as_list()


class FCN:
    """

    Base class for all 3d fully convolutional networks for semantic
    segmentation.

    """

    def __init__(self):
        self.session = None
        self.input_tensor = None
        self.input_shape = None
        self.output_tensor = None
        self.output_shape = None
        self.label_tensor = None
        self.logit_tensor = None
        self.loss_tensor = None
        self.loss_name = None
        self.loss_args = {} # additional arguments for loss
        self.weight_tensor = None
        self.border_size = None
        self.data_format = 'channels_last'
        self.reuse = False

    @staticmethod
    def _num_params():
        params = tf.trainable_variables()
        num_params = np.asarray(
            [np.asarray(x.get_shape().as_list()).prod() for x in params]).sum()
        return num_params

    def build_inference_model(self, inputs=None):
        pass

    def build_loss(self):
        pass

    def build_model(self, inputs, labels=None, weights=None):
        pass

    def train_step(self, optimizer, optimizer_args, global_step=None,
                   start_step=None, learning_rate=1e-5, decay_rate=0.97,
                   decay_steps=10000):
        if global_step is None:
            global_step = tf.train.get_or_create_global_step()
        if decay_rate < 1:
            if start_step is not None:
                ref_step = tf.subtract(global_step, start_step)
            else:
                ref_step = global_step
            learning_rate = tf.train.exponential_decay(learning_rate,
                                                       ref_step,
                                                       decay_steps,
                                                       decay_rate,
                                                       name="lr")
        else:
            learning_rate = tf.convert_to_tensor(learning_rate,
                                                 tf.float32)
        tf.summary.scalar('learning_rate', learning_rate)
        optimizer = optimizer(learning_rate, **optimizer_args)
        train_op = tf.contrib.layers.optimize_loss(
            self.loss_tensor,
            global_step=global_step,
            learning_rate=None,
            optimizer=optimizer,
            summaries=["loss", "learning_rate", "gradient_norm",
                       "global_gradient_norm"])
        return train_op

    def init_graph(self, sess=None):
        sess = self._get_session(sess)
        out = sess.run(tf.report_uninitialized_variables())
        if out.size > 0:
            sess.run(tf.global_variables_initializer())

    def predict(self, raw, output_tensor=None, sess=None, check_init=True):
        """
        Simple prediction of the raw input that requires the network to
        have the same input node size as the raw data.

        Args:
            raw: nd array
                Input data for the network that must have the same
                shape as self.input_tensor.
            sess: (Optional) A tensorflow session object
                If none then a session object is created and stored in
                self.session.
            check_init: (Optional) logical
                Check for uninitialized variables and init them.
                Use this then predict is called inside a loop to prevent
                memory leakage.
        Returns:
            out: nd array
                The network prediction.
        """

        sess = self._get_session(sess)
        raw = cube_to_input_shape(raw)
        if output_tensor is None:
            output_tensor = self.output_tensor
        if check_init:
            self.init_graph()
        return sess.run(output_tensor, {self.input_tensor: raw})

    def predict_dense(self, raw,
                      ckpt_file: str=None,
                      input_size: List[int]=None,
                      output_tensor=None,
                      sess=None):
        """
        Prediction for arbitrarily shaped cubes which is tiled to match the
        specified input size.
        Note that this function finalizes the graph to prevent any unexpected
        memory leakage.
        Args:
            raw: 3d or 4d array
                The input data for the network.
            ckpt_file: string
                Path to checkpoint file that is loaded for prediction.
            input_size: int array of length 3 or 4
                The shape of the input node to the network (see also
                create_pl_tensor).
                If not specified the inference graph needs to exist already
                (i.e. self.input_tensor and self.output_tensor must not be
                none). Otherwise the default graph will be reset and a new
                inference graph with input_size will be created.
            sess: (Optional) A tensorflow session object
                If none then a session object is created and stored in
                self.session.

        Returns:
            out: nd array
                The network prediction.
        """

        if input_size is None:
            if self.input_tensor is None:
                raise RuntimeError("Either construct the inference graph in "
                                   "advance or specify the input shape.")
            input_size = self.input_shape[1:]
        else:
            if len(input_size) == 3:
                input_size.append(1)
            tf.reset_default_graph()
            self._set_input(input_size)
            self.build_inference_model()
            self.output_shape = np.asarray(
                _get_shp(self.output_tensor))
            self.init_graph()

        if output_tensor is None:
            output_tensor = self.output_tensor

        output_tensor_shape = np.asarray(_get_shp(output_tensor))
        border_size = np.asarray(_get_shp(self.input_tensor))[1:4] \
                    - output_tensor_shape[1:4]

        input_size = np.asarray(input_size, np.int)

        if ckpt_file is not None:
            self.load_from_ckpt(ckpt_path=ckpt_file, sess=sess)

        # prediction shape
        pred_shape = np.asarray(raw.shape[:3] - border_size)
        out_tile_shape = np.asarray(input_size[:3] - border_size)

        # pad border
        out_shape = out_tile_shape * np.ceil(pred_shape / out_tile_shape)
        out_shape = np.asarray(out_shape, dtype=np.int)
        b = out_shape - pred_shape

        # allocate output
        out = ([1,], out_shape, output_tensor_shape[4:])
        out = np.zeros(np.concatenate(out))

        # pad raw to be divisible by input_size
        b_pad = [(0, x) for x in b]
        b_pad.append((0, 0))
        if raw.ndim < 4:
            raw = np.expand_dims(raw, axis=3)
        raw = np.pad(raw, b_pad, 'constant', constant_values=0)

        def batch(max_range):
            import itertools
            return itertools.product(*(range(j) for j in max_range))

        next_batch = batch(out_shape // out_tile_shape)
        tf.get_default_graph().finalize()  # just to make sure

        for i in next_batch:
            off = np.multiply(i, out_tile_shape)
            this_out = raw[off[0]:(off[0] + self.input_shape[1]),
                           off[1]:(off[1] + self.input_shape[2]),
                           off[2]:(off[2] + self.input_shape[3]), :]
            this_out = self.predict(this_out,
                                    output_tensor=output_tensor,
                                    check_init=False)

            out_ind = (0,
                       slice(off[0], off[0] + out_tile_shape[0]),
                       slice(off[1], off[1] + out_tile_shape[1]),
                       slice(off[2], off[2] + out_tile_shape[2])) + \
                      (slice(None),) * output_tensor_shape[4:].size
            out[out_ind] = this_out

        to = out.shape[1:4] - b
        out_ind = (slice(None),) + \
                  tuple(map(lambda u: slice(None, u), to)) + \
                  (slice(None),) * output_tensor_shape[4:].size
        return out[out_ind]

    def predict_wkw(self, wkw_in, off, shape, wkw_out=None, input_size=None,
                    f_pre=None, add_border=False):
        """
        Predict from a wkw dataset. Requires the wkw package.
        Note that this function finalizes the graph to prevent any unexpected
        memory leakage.

        Args:
            wkw_in: String to wkw.Dataset object to read the data from
            off: Offset for reading data
                see wkw.Dataset.read
            shape: Shape of cube to read
                see wkw.Dataset.read
            wkw_out: (Optional)
                string to wkw.Dataset object where result is stored
                If not specified output is returned as numpy array
            input_size: (Optional)
                Input size for the model if the inference graph is not yet build
            f_pre: (Optional)
                Function to preprocess data read from wkw dataset
            add_border: (Optional)
                Account for the cnet border in off and shape such that the
                prediction is in the bbox given by off and shape.

        Returns:
            dat_out: If wkw_out is not specified this is the prediction as numpy
                array. Otherwise, the off and shape for the predictions in the
                output wkw dataset are returned as a tuple.
        """

        import wkw
        from tqdm import tqdm
        off = np.asarray(off)
        shape = np.asarray(shape)
        if isinstance(wkw_in, str):
            dat_in = wkw.Dataset.open(wkw_in)
        else:
            dat_in = wkw_in
        if self.input_tensor is not None:
            pass
        elif input_size is not None:
            self.build_inference_model(input_size)
            self.init_graph()
        else:
            raise RuntimeError("Either construct the inference graph in "
                               "advance or specify the input shape.")

        spatial_in = self.spatial_input_size()
        if self.border_size is None:
            self.calculate_border_size()
        b = self.border_size
        spatial_out = spatial_in - b

        if add_border:
            off = np.asarray(off - (b / 2), dtype=np.int64)
            shape = np.asarray(shape + b, dtype=np.int64)
        bbox = [off, off + shape]

        if wkw_out is not None:
            if isinstance(wkw_out, str):
                dat_out = wkw.Dataset.open(wkw_out)
            else:
                dat_out = wkw_out
            print('Storing output in dataset {}'.format(dat_out.root))
        else:
            shp_out = bbox[1] - bbox[0] - b
            c = self._num_output_channels()
            dat_out = np.zeros(list(shp_out) + [c, ], dtype=np.float32)

        def tile_bbox(bbox, sptl_in, sptl_out, border_size):
            import itertools
            bbox = [np.asarray(x) for x in bbox]
            sptl_in = np.asarray(sptl_in)
            sptl_out = np.asarray(sptl_out)
            for start in itertools.product(
                    *(range(bbox[0][i], bbox[1][i] - border_size[i],
                            sptl_out[i]) for i in range(3))):
                bbox_in = [start, tuple(a + b for a, b in zip(start, sptl_in))]
                bbox_in = [np.asarray(x) for x in bbox_in]
                to = np.minimum(bbox[1] - bbox_in[0], sptl_in) - border_size
                bbox_out = [bbox_in[0] + border_size // 2,
                            bbox_in[0] + border_size // 2 + to]
                yield (bbox_in, to, bbox_out)

        tf.get_default_graph().finalize()  # just to make sure
        num_iter = np.product(np.ceil((shape - self.border_size) /
            np.asarray(spatial_out)))
        for (bbox_i, to, bbox_o) in tqdm(tile_bbox(bbox, spatial_in,
                                                   spatial_out, b),
                                         total=num_iter, ncols=80):
            raw = dat_in.read(bbox_i[0], spatial_in)
            raw = raw.transpose((1, 2, 3, 0))
            if f_pre is not None:
                raw = f_pre(raw)
            pred = self.predict(raw, check_init=False)
            pred = np.squeeze(pred, axis=0)
            pred = pred[:to[0], :to[1], :to[2], :]  # assumes channels last
            if wkw_out is not None:
                pred = np.transpose(pred, (3, 0, 1, 2))  # channels first
                dat_out.write(bbox_o[0], pred)
            else:
                off = bbox_o[0] - (bbox[0] + b // 2)
                to = np.minimum(off + to, dat_out.shape[:3])
                dat_out[off[0]:to[0], off[1]:to[1], off[2]:to[2], :] = pred

        if wkw_out is not None:
            dat_out = (bbox[0] + b // 2, shape - b)
        return dat_out

    def loss(self):
        print('Adding {} loss layer.'.format(self.loss_name))

        if self.loss_name == 'custom':
            # TODO: test if this works
            if not all(k in self.loss_args for k in ('func', 'args')):
                print(
                    'loss_args dictionary must contain \'func\' and \'args\'.')
            loss_t = self.loss_args['func'](self.label_tensor,
                                            self.output_tensor,
                                            self.logit_tensor,
                                            self.weight_tensor,
                                            *self.loss_args['args'])
            if 'name' in self.loss_args:
                tf.summary.scalar('loss/' + self.loss_args['name'], loss_t)
            else:
                print('Adding loss as custom in tensorboard. \
                       You can specify a custom name in loss_args \
                       [\'name\']')
                tf.summary.scalar('loss/custom', loss_t)
        elif self.loss_name == 'squared':

            # always report regular l2 loss
            with tf.variable_scope('loss/l2'):
                loss_t = tf.multiply(tf.constant(0.5, tf.float32),
                                     tf.losses.mean_squared_error(
                                         self.label_tensor,
                                         self.output_tensor))
                tf.summary.scalar('loss/l2', loss_t)

            # if weights are used then use the weighted l2 loss
            if self.weight_tensor is not None:
                with tf.variable_scope('loss/l2_weighted'):
                    loss_t = tf.multiply(tf.constant(0.5, tf.float32),
                                           tf.losses.mean_squared_error(
                                            self.label_tensor,
                                            self.output_tensor,
                                            weights=self.weight_tensor,
                                            reduction=tf.Reduction.SUM_OVER_BATCH_SIZE))
                    tf.summary.scalar('loss/l2_weighted/', loss_t)

        elif self.loss_name == 'one_sided_squared':
            with tf.variable_scope('loss/l2'):
                loss_t = tf.multiply(tf.constant(0.5, tf.float32),
                                     tf.losses.mean_squared_error(
                                         self.label_tensor,
                                         self.output_tensor))
                tf.summary.scalar('loss/l2/', loss_t)
            with tf.variable_scope('loss/one_sided_l2'):
                if self.weight_tensor is None:
                    self.weight_tensor = tf.ones_like(self.output_tensor,
                            dtype=tf.float32)

                # set weights for elements below -1 and above 1 to zero if
                # the correspond labels are -1 and 1, respectively
                weights = self.weight_tensor
                lower = tf.logical_and(tf.less_equal(self.output_tensor, -1),
                                       tf.less_equal(self.label_tensor, -1))
                upper = tf.logical_and(tf.greater_equal(self.output_tensor, 1),
                                       tf.greater_equal(self.label_tensor, 1))
                mask = tf.to_float(tf.logical_not(tf.logical_or(lower, upper)))
                weights = tf.multiply(weights, mask)
                loss_t = tf.multiply(tf.constant(0.5, tf.float32),
                                     tf.losses.mean_squared_error(
                                         self.label_tensor,
                                         self.output_tensor,
                                         weights=weights,
                                         reduction=tf.Reduction.SUM_OVER_BATCH_SIZE))
                tf.summary.scalar('loss/one_sided_l2/', loss_t)

        elif self.loss_name == 'full_gaussian_nll':

            with tf.variable_scope('full_gaussian_nll'):

                if self.data_format == 'channels_last':
                    dim = self.output_shape[-1]
                    assert(dim % 2 == 0)
                    dim = dim // 2
                    with tf.variable_scope('get_mean'):
                        mean = self.output_tensor[:, :, :, :, :dim]
                    with tf.variable_scope('get_log_std'):
                        if 'model_sig' in self.loss_args and not self.loss_args['model_sig']:
                            # for debugging
                            log_std = tf.zeros_like(mean, dtype=tf.float32)
                        else:
                            log_std = self.output_tensor[:, :, :, :, dim:]
                else:
                    dim = self.output_shape[1]
                    assert(dim % 2 == 0)
                    dim = dim // 2
                    with tf.variable_scope('get_mean'):
                        mean = self.output_tensor[:, :dim, :, :, :]
                    with tf.variable_scope('get_log_std'):
                        if 'model_sig' in self.loss_args and not self.loss_args['model_sig']:
                            # for debugging
                            log_std = tf.zeros_like(mean, dtype=tf.float32)
                        else:
                            log_std = self.output_tensor[:, dim:, :, :, :]

                with tf.variable_scope('squared_term'):
                    eps = 0.5 * tf.exp(-2. * log_std) * tf.square(mean - self.label_tensor)
                    tf.summary.scalar('loss/squared_term', tf.reduce_mean(eps))
                    if self.weight_tensor is not None:
                        eps = tf.multiply(self.weight_tensor, eps, name='mult_weight')

                loss_t = tf.add(log_std, eps, name='add_log_std')
                #if self.weight_tensor is not None:
                #    loss_t = tf.multiply(self.weight_tensor, loss_t, name='mult_weight')
                loss_t = tf.reduce_mean(loss_t)
                tf.summary.scalar('loss/full_gaussian_nll', loss_t)
                tf.summary.histogram('output_mean', mean)
                tf.summary.histogram('output_log_std', log_std)
                tf.summary.histogram('label', self.label_tensor)

            with tf.name_scope('image_summaries/'):
                out_shape = np.concatenate(([1, ], self.spatial_output_size(), [1, ]))
                out_shape[3] = 1
                if self.data_format == 'channels_first':
                    out_shape = out_shape[[0, 4, 1, 2, 3]]
                std_out = tf.exp(tf.slice(log_std, [0, 0, 0, 0, 0], out_shape))
                std_out = tf.squeeze(std_out, 4)
                tf.summary.image('prediction_std', std_out)

            # report regular squared error for comparison
            with tf.variable_scope('loss/l2'):
                loss = 0.5 * tf.losses.mean_squared_error(self.label_tensor, mean)
                tf.summary.scalar('loss/l2/', loss)

            # if weights are used then also report the weighted l2 loss
            if self.weight_tensor is not None:
                with tf.variable_scope('loss/l2_weighted'):
                    loss = 0.5 * tf.losses.mean_squared_error(
                            self.label_tensor, mean,
                            weights=self.weight_tensor,
                            reduction=tf.Reduction.SUM_OVER_BATCH_SIZE)
                    tf.summary.scalar('loss/l2_weighted/', loss)

        elif self.loss_name == 'cross-entropy':
            if self.weight_tensor is None:
                loss_t = tf.losses.sparse_softmax_cross_entropy(
                    self.label_tensor, self.logit_tensor)
            else:
                loss_t = tf.losses.sparse_softmax_cross_entropy(
                    self.label_tensor, self.logit_tensor,
                    weights=self.weight_tensor,
                    reduction=tf.Reduction.SUM_OVER_BATCH_SIZE)
            tf.summary.scalar('loss/cross-entropy/', loss_t)

        elif self.loss_name == 'dice':
            c_dim = 4 if (self.data_format == 'channels_last') else 1
            # label to one-hot if necessary
            sy = np.asarray(self.output_tensor.get_shape().as_list())
            depth = sy[c_dim]
            sp = np.asarray(self.label_tensor.shape.as_list())
            if sp[c_dim] == 1 and sp[c_dim] < sy[c_dim]:
                labels = tf.one_hot(self.label_tensor, depth=depth, axis=c_dim,
                                    dtype=tf.float32)
                labels = tf.squeeze(labels, axis=c_dim + 1)
            else:
                labels = self.label_tensor
            loss_t = codat.training.losses.tversky_loss(labels,
                                                        self.output_tensor)
            tf.summary.scalar('loss/dice_loss/', loss_t)

        elif "absolute":
            with tf.variable_scope('loss/l1'):
                loss_t = tf.losses.mean_squared_error(self.label_tensor,
                                                      self.output_tensor)
                tf.summary.scalar('loss/l1/', loss_t)
            if self.weight_tensor is not None:
                with tf.variable_scope('loss/l1_weighted'):
                    loss_t = tf.losses.mean_squared_error(
                        self.label_tensor, self.output_tensor,
                        weights=self.weight_tensor,
                        reduction=tf.Reduction.SUM_OVER_BATCH_SIZE)
                    tf.summary.scalar('loss/l1_weighted/', loss_t)

        else:
            raise RuntimeError('Unrecognized loss {}'.format(self.loss_name))

        # add regularization losses
        reg_variables = tf.get_collection(tf.GraphKeys.REGULARIZATION_LOSSES)
        if len(reg_variables) > 0:
            reg = tf.losses.get_regularization_loss()
            loss_t = loss_t + reg
            tf.summary.scalar('loss/regularized_loss/', loss_t)
        return loss_t

    def load_from_ckpt(self, ckpt_path, sess=None, **kwargs):
        sess = self._get_session(sess)
        saver = tf.train.Saver(**kwargs)
        if os.path.isdir(ckpt_path):
            ckpt_path = self.get_latest_ckpt(ckpt_path)
        saver.restore(sess, ckpt_path)

    def save_to_ckpt(self, ckpt_path, sess=None):
        sess = self._get_session(sess)
        saver = tf.train.Saver()
        save_path = saver.save(sess, ckpt_path)
        print("Model saved in file: {}".format(save_path))

    def _set_input(self, inputs):
        self.input_tensor = self.create_pl_tensor(inputs)
        self.input_shape = np.asarray(_get_shp(self.input_tensor))

    def _set_output(self, output_tensor):
        self.output_tensor = output_tensor
        self.output_shape = np.asarray(_get_shp(self.output_tensor))

    def _get_session(self, sess=None):
        if sess is None:
            if self.session is None:
                sess = tf.Session()
                self.session = sess
            else:
                sess = self.session
        else:
            self.session = sess
        return sess

    def spatial_input_size(self):
        if self.data_format == 'channels_last':
            siz = self.input_shape[1:-1]
        else:
            siz = self.input_shape[2:]
        return siz

    def spatial_output_size(self):
        if self.data_format == 'channels_last':
            siz = self.output_shape[1:-1]
        else:
            siz = self.output_shape[2:]
        return siz

    def get_spatial_shape(self, t):
        shp = np.asarray(_get_shp(t))
        if self.data_format == 'channels_last':
            shp = shp[1:-1]
        else:
            shp = shp[2:]
        return shp

    def _num_output_channels(self):
        if self.data_format == 'channels_last':
            c = self.output_shape[-1]
        else:
            c = self.output_shape[1]
        return c


    def calculate_border_size(self, inputs=None):
        """
        Calculate the FCN border size form the input and output shape.
        Args:
            inputs: Input to build_inference_model if not yet built.
        """

        if inputs is not None:
            self.build_inference_model(inputs)
        elif self.input_shape is not None and self.output_shape is not None:
            pass
        elif self.input_shape is None or self.output_shape is None:
            raise RuntimeError("Input shape or output shape not set.")
        else:
            raise RuntimeError("Could not determine border size.")
        border_size = self.input_shape - self.output_shape
        self.border_size = border_size[1:-1]

    def calculate_border_brute_force(self, inputs):
        """
        Border size calculation hack that completely clears the graph after
        border calculation. Should be used only directly after FCN init
        and all tensors (also placeholders etc.) need to be defined afterwards.
        Args:
            inputs: Input to build_infererence_model
        """

        self.calculate_border_size(inputs)
        tf.reset_default_graph()

    def image_summaries(self, border_size):
        """
        Create summaries for input, output and labels.
        Returns:

        """
        output_shape = self.output_tensor.get_shape().as_list()
        input_shape = self.input_tensor.get_shape().as_list()

        with tf.name_scope('image_summaries'):
            outs = output_shape
            ins = input_shape

            # output example
            if self.data_format == 'channels_last':
                im_out = tf.slice(self.output_tensor, [0, 0, 0, 0, 0],
                                  [1, outs[1], outs[2], 1, 1])
            else:
                im_out = tf.slice(tf.transpose(self.output_tensor,
                                               perm=[0, 2, 3, 4, 1]),
                                  [0, 0, 0, 0, 0],
                                  [1, outs[1], outs[2], 1, 1])
            im_out = tf.squeeze(im_out, 4)
            im_out = tf.divide(
                tf.clip_by_value(im_out, -1, 1) + tf.constant([1.]),
                tf.constant([2.]))
            if self.data_format == 'channels_last':
                label_out = tf.slice(self.label_tensor, [0, 0, 0, 0, 0],
                                     [1, outs[1], outs[2], 1, 1])
            else:
                label_out = tf.slice(tf.transpose(self.label_tensor,
                                                  perm=[0, 2, 3, 4, 1]),
                                     [0, 0, 0, 0, 0],
                                     [1, outs[1], outs[2], 1, 1])
            label_out = tf.squeeze(label_out, 4)
            if self.loss_name in str(['squared', 'full_gaussian_nll']):
                label_out = tf.divide(tf.clip_by_value(label_out, -1, 1) +
                                      tf.constant([1.]), tf.constant([2.]))
            else:
                label_out = tf.cast(label_out, tf.float32)
            im = tf.concat([im_out, label_out], 2)
            tf.summary.image('prediction/', im)

            im_in = tf.slice(self.input_tensor,
                             np.asarray([0, 0, 0, border_size[2] / 2, 0],
                                        dtype=np.int32),
                             [1, ins[1], ins[2], 1, 1])
            im_in = tf.squeeze(im_in, 4)
            tf.summary.image('input/', im_in)

    def create_pl_tensor(self, inputs, dtype=tf.float32):
        return create_pl_tensor(inputs, dtype=dtype,
                                data_format=self.data_format)

    def create_pl_label_tensor(self):

        if self.loss_name in ['cross-entropy', 'tversky']:
            self.label_tensor = create_pl_tensor(
                self.spatial_output_size(), dtype=tf.int32,
                data_format=self.data_format)
        else:
            self.label_tensor = create_pl_tensor(
                self.output_tensor.get_shape().as_list(),
                data_format=self.data_format)

        return self.label_tensor

    @staticmethod
    def get_latest_ckpt(folder):
        import re
        r = re.compile('.*/model.ckpt-Final.*')
        files = sorted(glob.glob(os.path.join(folder, 'model*.data-*')),
                       key=os.path.getctime)
        final = [x for x in files if r.match(x)]
        if len(final) > 0:
            file = final[0]  # sometimes final is saved earlier
        else:
            file = files[-1]  # simply take last one

        file = os.path.splitext(file)[0]
        return file

    @staticmethod
    def from_config(config_file: str, construct_graph=True, load_ckpt=True,
                    config_dict=None, **kwargs):
        """
        Construct a FCN object from a config file.

        Args:
            config_file: Full path to config file (will be executed with exec
                removing a potential if __name__ == '__main__' part)
            construct_graph: Construct the tensorflow computation graph.
            load_ckpt: Load the latest checkpoint file from the output dir
                (to specify an alternative config file see kwargs).
            config_dict: Dictionary object that is updated with the local
                dictionary from exec.
            **kwargs: Overwrite any config file parameters.
                Use 'config_file' to the full path to a config file.

        Returns:
            fcn: The resulting FCN child object.

        """
        pass
