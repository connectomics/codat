""" Default CNN for voxelwise segmentation

@author: Benedikt Staffler <benedikt.staffler@brain.mpg.de

tested with tf1.4

"""

from codat.nn.fcn import FCN
import codat.nn.util as util
import numpy as np
import scipy.io as sio
import tensorflow as tf
import re
import os


def dilation_rate(max_pool, stride):
    """
    Calculate the dilation rate based on a models stride/max pool operations.
    Args:
        max_pool: [Nx3] int array
            Max pooling window in each layer (excluding input layer).
        stride: [Nx3] int array
            Stride for convolution in each layer (excluding input layer).
    Returns:
        dil_rate: [Nx3] int array
            Total dilation rate after each layer including input layer.
    """

    dil_rate = np.cumprod(np.multiply(max_pool, stride), 0)
    dil_rate = np.concatenate((np.asarray([[1, 1, 1], ]), dil_rate[:-1, :]))
    return dil_rate


def get_weights(param_file):
    """
    Load weights from a matfile.
    Args:
        param_file: string
            Path to matfile. The matfile should contain a cell array w with the
             weights and a cell array b with the biases for each layer.

    Returns:
        w: list
            List with the weights for each layer
        b: list
            List with the biases for each layer

    """

    matfile = sio.loadmat(param_file)
    w_mat = matfile["w"].flatten()
    b_mat = matfile["b"].flatten()
    w = list()
    b = list()
    for i in range(1, w_mat.shape[0]):
        if w_mat[i].ndim < 5:
            w_mat[i] = np.expand_dims(w_mat[i], 4)
        w_mat[i] = w_mat[i][:, :, :, ::-1, :]
        for j in range(w_mat[i].shape[3]):
            for k in range(w_mat[i].shape[4]):
                tmp = w_mat[i][:, :, :, j, k]
                shp = tmp.shape
                tmp = tmp.ravel()
                tmp = tmp[::-1]
                tmp = tmp.reshape(shp)
                w_mat[i][:, :, :, j, k] = tmp

        b_mat[i] = b_mat[i].astype(np.float32).reshape([1, 1, 1, b_mat[i].size])
        w.append(w_mat[i])
        b.append(b_mat[i])

    return w, b


def _cube_to_input_shape(inputs):
    if inputs.ndim == 3:  # 3d input cube (note it will also go in == 4 if)
        inputs = np.expand_dims(inputs, 3)
    if inputs.ndim == 4:  # 4d input cube
        inputs = np.expand_dims(inputs, 0)
    return inputs


class VanillaCNN(FCN):

    def __init__(self,
                 feature_maps: list,
                 kernel_shape: list,
                 act_hidden=(util.tanh_scaled, ),
                 act_out=None,
                 max_pool=None,
                 stride=None,
                 dropout=None,
                 loss='squared',
                 padding=None):
        """
        Simple vanilla CNN architecture.
        Args:
            feature_maps: list
                Number of feature maps for each hidden and the output layer.
            kernel_shape: list of lists
                Kernel shape for the 3d convolution for each convolutional
                layer.
            max_pool: list of lists
                Max-pooling window for each layer. Must have same shape as
                kernel_size or the same shape for all layer.
            stride: list of lists
                Max-pooling window for each layer. Must have same shape as
                kernel_size or the same shape for all layer.
            dropout: list
                Dropout fraction for each layer that is applied to the input of
                the corresponding layer. Actually refers to the keep fraction.
        """

        FCN.__init__(self)
        self.session = None
        self.feature_maps = feature_maps
        self.num_layer = len(feature_maps) + 1
        self.kernel_shape = np.asarray(kernel_shape, dtype=np.uint32)
        self.data_format = 'channels_last'

        if self.kernel_shape.shape[0] == 1:
            self.kernel_shape = np.repeat(self.kernel_shape, self.num_layer - 1,
                                          0)

        if max_pool is not None:
            self.max_pool = np.asarray(max_pool, dtype=np.uint32)
            if self.max_pool.shape[0] == 1:
                self.max_pool = np.repeat(self.max_pool, self.num_layer - 1, 0)
        else:
            self.max_pool = np.ones((len(feature_maps), 3), dtype=np.uint32)

        if stride is not None:
            self.stride = np.asarray(stride, dtype=np.uint32)
            if self.stride.shape[0] == 1:
                self.stride = np.repeat(self.stride, self.num_layer - 1, 0)
        else:
            self.stride = np.ones((len(feature_maps), 3), dtype=np.uint32)

        if dropout is not None:
            self.dropout = np.asarray(dropout, dtype=np.float32).ravel()
            if self.dropout.shape[0] == 1:
                self.dropout = np.repeat(self.dropout, self.num_layer - 1)
        else:
            self.dropout = None

        activation = act_hidden
        if len(act_hidden) == 1:
            activation = [act for act in act_hidden
                          for _ in range(self.num_layer - 2)]
        elif len(act_hidden) < self.num_layer - 2:
            raise RuntimeError("Hidden activation must be specified for hidden"
                               "each layer.")

        self.dil_rate = dilation_rate(self.max_pool, self.stride)
        if padding is None:
            self.padding = "valid"
        else:
            self.padding = padding
        self.border_size = self.calc_border_size()

        self.loss_name = loss
        if act_out is None:
            if self.loss_name == 'squared':
                activation.append(util.tanh_scaled)
            elif self.loss_name == 'cross-entropy':
                activation.append(tf.nn.softmax)
        elif act_out == "linear":
            activation.append(None)

        if loss not in ['squared', 'cross-entropy']:
            raise RuntimeError("Unknown loss {}".format(loss))

        self.activation = activation
        self.input_tensor = None
        self.input_shape = None
        self.output_tensor = None
        self.output_shape = None
        self.label_tensor = None
        self.label_shape = None
        self.weight_tensor = None
        self.weight_shape = None
        self.net = None
        self.loss_tensor = None
        self.logit_tensor = None

    def build_model(self, inputs, labels=None, weights=None, name=None):
        return self.model(inputs, labels, weights, name=name)

    def model(self, inputs, labels=None, weights=None, param_file=None,
              name=None):
        """
        Create the computational graph.
        Args:
            inputs: list or tensor
                Inputs tensor or list of input size. In the latter case a
                placeholder for the labels is generated.
                (see also create_pl_tensor).
            labels:  tensor
                Label tensor. If none then a placeholder with the correct shape
                will be created
            weights: tensor
                Weight tensor of same size as labels.
            param_file: string
                Path to matlab parameter file.
            name: string
                Name of the network used as a prefix for all tensors.
        """

        with tf.variable_scope(name if name is not None else ''):
            self._set_input(inputs)
            self.input_shape = \
                np.asarray(self.input_tensor.get_shape().as_list())
            self.net, self.output_tensor, self.logit_tensor =\
                self.inference(param_file)
            self.output_shape = \
                np.asarray(self.output_tensor.get_shape().as_list())
            if weights is not None:
                self.weight_tensor = weights
                self.weight_shape = np.asarray(self.weight_tensor.get_shape().
                                               as_list())
            if labels is None:
                if self.loss_name == 'squared':
                    self.label_tensor = util.create_pl_tensor(
                        tuple(self.output_shape))
                elif self.loss_name == 'cross-entropy':
                    tmp = np.copy(self.output_shape)
                    tmp[-1] = 1
                    self.label_tensor = util.create_pl_tensor(tuple(tmp),
                                                              tf.int32)
            else:
                self.label_tensor = util.create_pl_tensor(labels)

            self.label_shape = np.asarray(
                self.label_tensor.get_shape().as_list(),
                dtype=np.uint32)
            self.loss_tensor = self.loss()

    def calculate_border_size(self, inputs=None):
        self.calc_border_size()

    def calculate_border_brute_force(self, inputs=None):
        self.calc_border_size()

    def calc_border_size(self):
        border = np.array([0, 0, 0], dtype=np.uint32)
        if self.padding == "valid":
            d = self.dil_rate
            k_s = np.concatenate((np.array([[0, 0, 0], ]), self.kernel_shape),
                                 axis=0)
            mp = np.concatenate((np.array([[1, 1, 1], ]), self.max_pool), axis=0)
            st = np.concatenate((np.array([[1, 1, 1], ]), self.stride), axis=0)
            for lyr in range(1, self.num_layer):
                border = border + np.multiply(d[lyr - 1], k_s[lyr] - 1) + \
                                + np.multiply(d[lyr - 1],
                                              np.multiply(st[lyr], mp[lyr] - 1))

            if np.any(border % 2):
                RuntimeError("Border size must be symmetric around target voxel,"
                             "i.e. an even number")
            self.border_size = np.int64(border)
        return np.int64(border)

    def build_inference_model(self, inputs=None):
        if inputs is not None:
            self.input_tensor = util.create_pl_tensor(inputs)
        self.inference()

    def inference(self, param_file=None):

        if param_file is not None:
            w, b = get_weights(param_file)
        else:
            w = None
            b = None

        net = list()
        net.append(self.input_tensor)
        h = self.input_tensor
        print("Adding input layer of shape {}".format(self.input_shape))
        init_w = tf.contrib.layers.xavier_initializer()
        init_b = tf.contrib.layers.xavier_initializer()
        for lyr in range(len(self.feature_maps)):
            if param_file is not None:
                init_w = tf.constant_initializer(w[lyr])
                init_b = tf.constant_initializer(b[lyr])
            name = 'layer_{:02d}'.format(lyr + 1)
            if self.dropout is not None and self.dropout[lyr] < 1:
                h = tf.nn.dropout(h, self.dropout[lyr], name=name + '/dropout')
                dr_str = "dropout {:.2f}".format(1 - self.dropout[lyr])
            else:
                dr_str = "no dropout"
            if lyr < len(self.feature_maps) - 1:
                h = tf.layers.conv3d(h, self.feature_maps[lyr],
                                     self.kernel_shape[lyr],
                                     data_format=self.data_format,
                                     dilation_rate=self.dil_rate[lyr],
                                     activation=self.activation[lyr],
                                     kernel_initializer=init_w,
                                     bias_initializer=init_b,
                                     name=name + '/conv',
                                     padding=self.padding)
            else:
                h = tf.layers.conv3d(h, self.feature_maps[lyr],
                                     self.kernel_shape[lyr],
                                     data_format=self.data_format,
                                     dilation_rate=self.dil_rate[lyr],
                                     activation=None,
                                     kernel_initializer=init_w,
                                     bias_initializer=init_b,
                                     name=name + '/conv_logits',
                                     padding=self.padding)
            net.append(h)
            if np.any(self.max_pool[lyr] > 1):
                h = tf.layers.max_pooling3d(h, self.max_pool[lyr] +
                                            self.dil_rate[lyr] - 1, 1,
                                            data_format=self.data_format,
                                            name=name + '/max_pool',
                                            padding=self.padding)
                net.append(h)
            if lyr == len(self.feature_maps) - 1:
                logits = h
                if self.activation[-1] is not None:
                    with tf.name_scope(name + "/outputs"):
                        h = self.activation[-1](h)
                act_name = "linear"
            else:
                 act_name = self.activation[lyr].__name__

            print("Adding {}: output shape {}, "
                  "kernel size {}, "
                  "max-pool {}, "
                  "activation {}, ".format(name,
                                           h.get_shape().as_list(),
                                           self.kernel_shape[lyr],
                                           self.max_pool[lyr],
                                           act_name) + dr_str)

        y = h
        return net, y, logits

    def weights_to_matfile(self, mat_file, ckpt_path=None):
        """
        Save the network weights to a matfile.
        Args:
            mat_file: string
                Path to output file.
            ckpt_path: (Optional) string
                Path to checkpoint file to load. If the training output folder
                is specified the newest checkpoint file is automatically selected.

        Returns:
            params: list
                Network parameters in list (result of
                sess.run(tf.trainable_variables())
        """

        if ckpt_path is not None:
            if os.path.isdir(ckpt_path):  # for storing in matfile
                ckpt_path = self.get_lastest_ckpt(ckpt_path)
            self.load_from_ckpt(ckpt_path=ckpt_path)

        params = tf.trainable_variables()
        params_v = self.session.run(params)
        sio.savemat(mat_file, {'params': params_v, 'ckpt_file': ckpt_path})

    @staticmethod
    def _get_kernels():
        tmp = tf.trainable_variables()
        # w = tmp[0::2]
        pattern = re.compile("layer_\d/conv/kernel*")
        w = [p for p in tmp if pattern.match(p.name)]
        return w

    @staticmethod
    def _get_biases():
        tmp = tf.trainable_variables()
        # b = tmp[1::2]
        pattern = re.compile("layer_\d/conv/bias*")
        b = [p for p in tmp if pattern.match(p.name)]
        return b
