"""
Training utility functions.

@author: Benedikt Staffler <benedikt.staffler@brain.mpg.de

tested with tf1.4

"""


import tensorflow as tf
import time
from datetime import datetime
from typing import Dict
from shutil import copyfile
import os
import fnmatch


class LoggerHook(tf.train.SessionRunHook):
    """Logs loss and runtime."""

    def __init__(self,
                 loss_tensor: tf.Tensor,
                 log_frequency: int = 1,
                 log_dict: Dict[str, tf.Tensor]=None):
        """
        
        Args:
            loss_tensor: tensor of the network loss
            log_frequency: logging frequency in number of training steps
            **kwargs: Name-tensor pairs that are logged in addition to loss.
        """

        self._step = -1
        self._start_time = None
        self.log_frequency = log_frequency
        if log_dict is None:
            log_dict = {'loss': loss_tensor}
        else:
            log_dict.update({'loss': loss_tensor})
        self.log_objects = log_dict

    def begin(self):
        self._step = -1
        self._start_time = time.time()

    def before_run(self, run_context):
        self._step += 1
        return tf.train.SessionRunArgs(self.log_objects)

    def after_run(self, run_context, run_values):
        if self._step % self.log_frequency == 0:
            current_time = time.time()
            duration = current_time - self._start_time
            self._start_time = current_time

            log_values = run_values.results
            examples_per_sec = self.log_frequency / duration
            sec_per_batch = float(duration / self.log_frequency)

            format_str = (
                '%s: step %d, loss= %.2f (%.1f examples/sec; %.3f '
                'sec/batch)')
            for key, value in log_values.items():
                if not key == 'loss':
                    format_str += ", {}= {:.2f}".format(key, value)
            print(format_str % (datetime.now(), self._step, log_values['loss'],
                                examples_per_sec, sec_per_batch))


class MaxRuntimeHook(tf.train.SessionRunHook):
    """
    Stop after a maximal runtime in seconds
    """

    def __init__(self, max_runtime=None):
        self.max_runtime = max_runtime
        self._start_time = time.time()

    def begin(self):
        self._start_time = time.time()

    def after_run(self, run_context, run_values):
        if time.time() - self._start_time >= self.max_runtime:
            print('Maximum runtime of {} seconds reached. Stopping training.'
                  .format(self.max_runtime))
            run_context.request_stop()


class SaveAtEndHook(tf.train.SessionRunHook):
    """
    Save at the end of training
    """

    def __init__(self, output_folder):
        self._saver = None
        self.output_folder = output_folder

    def begin(self):
        self._saver = tf.train.Saver()

    def end(self, session):
        print('Saving final parameters.')
        self._saver.save(session, os.path.join(self.output_folder,
                                               'model.ckpt-Final'))


def create_output_folder(folder, overwrite=False):
    if os.path.exists(folder):
        if overwrite:
            print('Output directory {} already exists.'.format(folder))
            for file in os.listdir(folder):
                if fnmatch.fnmatch(file, 'model.ckpt*'):
                    print('A checkpoint file  was found and is potentially '
                          'loaded for training.')
                    break
        else:
            raise RuntimeError("Output directory {} already exists and"
                               "overwrite is false.".format(folder))
    else:
        print('Saving output to {}'.format(folder))
        os.makedirs(folder)


def copy_config_to(config_file, output_dir):
    copyfile(config_file, os.path.join(output_dir, 'config.py'))
