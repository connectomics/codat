""" Code related to loss functions.

@author: Benedikt Staffler <benedikt.staffler@brain.mpg.de

"""

import tensorflow as tf
import numpy as np


def tversky_loss(y: tf.Tensor,
                 y_pred: tf.Tensor,
                 c_dim=4,
                 alpha=0.5,
                 beta=0.5,
                 smooth=1.,
                 name='dice_loss') -> tf.Tensor:
    """
    Implementation of the Tversky loss
    (see Salehi et al., 2017, MLMI, https://arxiv.org/abs/1706.05721)
    which generalizes the Dice and Tanimoto index:
    alpha=beta=0.5  : dice coefficient
    alpha=beta=1    : tanimoto coefficient
    alpha+beta=1    : F_beta score
    The Tversky loss is calculated separately for each class and added together.
    The loss is normalized to [0, 1] using the number of classes.
    
    Use larger beta to weigh recall higher than precision.

    
    Args:
        y: The labels as one-hot encoding with the same size as y_pred.
        y_pred: The tensor of the network predictions.
        c_dim: The index of the channel dimension in y.
        alpha: Tversky loss alpha.
        beta: Tversky loss beta.
        smooth: Optional smoothing that is added to the nominator and
            denominator of the Tversky loss.

    Returns:
        loss: The loss tensor.
        
    """

    # convert to one-hot if shapes are not equal
    sp = np.asarray(y_pred.shape.as_list())

    # reduce all except channel dimension
    red_dim = list(np.setdiff1d(np.arange(0, 5), 4))

    with tf.name_scope(name):
        n_classes = tf.constant(sp[c_dim], dtype=tf.float32, name='num_classes')
        alpha = tf.constant(alpha, dtype=tf.float32, name='alpha')
        beta = tf.constant(beta, dtype=tf.float32, name='beta')
        smooth = tf.constant(smooth, dtype=tf.float32, name='smooth')
        with tf.name_scope('numerator'):
            num = tf.reduce_sum(tf.multiply(y, y_pred), axis=red_dim) + smooth

        with tf.name_scope('denominator'):
            red_sum = lambda x: tf.reduce_sum(x, axis=red_dim)
            denom = num + \
                    alpha * red_sum(tf.multiply(1 - y, y_pred)) + \
                    beta * red_sum(tf.multiply(y, 1 - y_pred))
        loss_t = (1 - tf.reduce_sum(num / denom) / n_classes)
    return loss_t
