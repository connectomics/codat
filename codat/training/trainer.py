""" Fully convolutional neural network trainer.

@author: Benedikt Staffler <benedikt.staffler@brain.mpg.de

tested with tf1.4

"""

import tensorflow as tf
import codat.training.util
from typing import Dict


def train_model_mts(cnet,
                    optimizer,
                    optimizer_args: Dict,
                    global_step=None,
                    learning_rate=None,
                    lr_decay_rate=1,
                    decay_steps=10000,
                    add_weight_summaries=False,
                    add_image_summaries=False,
                    custom_summaries=None,
                    log_frequency=10000,
                    max_steps=1e8,
                    max_runtime=1e8,
                    output_dir=None,
                    save_ckpt_secs=3600,
                    save_summ_steps=10000,
                    log_count_steps=10000,
                    ckpt_file=None,
                    max_to_keep=5):
    """
    Trainer for monitored training session.
    
    Args:
        cnet: A FCN object.
        optimizer: The optimizer used in train_step of FCN.
        optimizer_args: Additional arguments passed to the optimizer.
        global_step: global step tensor
            (default: tf.train.get_or_create_global_step)
        learning_rate: (float) learning rate
        lr_decay_rate: (float) learning rate decay
            (see tf.train.exponential_decay)
        decay_steps: Reference number of steps for the decay rate
            (see tf.train.exponential_decay)
        add_weight_summaries: Flag to call the fcn.image_summaries function.
        add_image_summaries: Flag to add histograms for all trainable variables.
        custom_summaries: Function that will be called with the fcn network as
            input after graph construction to create custom summaries. 
        log_frequency: 
        max_steps: 
        max_runtime: 
        output_dir: 
        save_ckpt_secs: 
        save_summ_steps: 
        log_count_steps: 
        ckpt_file: (Optional) checkpoint file to load at the beginning of
            training
        max_to_keep: Number of checkpoints that are kept. If != 5 this currently
            results in a strange log message which however does not seem to
            cause any trouble.
    """

    if global_step is None:
        global_step = tf.train.get_or_create_global_step()
    start_step = tf.Variable(0, name='start-step', trainable=False,
                             dtype=tf.int64)

    # get training step
    train_step = cnet.train_step(optimizer, optimizer_args,
                                 global_step=global_step,
                                 start_step=start_step,
                                 learning_rate=learning_rate,
                                 decay_rate=lr_decay_rate,
                                 decay_steps=decay_steps)

    # weight distributions
    if add_weight_summaries:
        params = tf.trainable_variables()
        for i in range(len(params)):
            tf.summary.histogram('params/' + params[i].name.replace(":", "_"),
                                 params[i])

    # image summaries
    if add_image_summaries:
        cnet.image_summaries(cnet.border_size)

    # custom summaries
    if custom_summaries is not None:
        custom_summaries(cnet)

    # hooks
    log_hook = codat.training.util.LoggerHook(cnet.loss_tensor,
                                              log_frequency=log_frequency)
    stop_iter_hook = tf.train.StopAtStepHook(last_step=max_steps)
    max_runtime_hook = codat.training.util.MaxRuntimeHook(max_runtime)
    save_at_end_hook = codat.training.util.SaveAtEndHook(output_dir)

    saver = tf.train.Saver(var_list=tf.trainable_variables() + [global_step, ])
    # hack to set start_step
    saver_start_step = tf.train.Saver({'global_step': start_step})
    scaffold = None
    if max_to_keep != 5:
        scaffold = tf.train.Scaffold(saver=tf.train.Saver(max_to_keep=max_to_keep))
    with tf.train.MonitoredTrainingSession(
            checkpoint_dir=output_dir,
            save_checkpoint_secs=save_ckpt_secs,
            save_summaries_steps=save_summ_steps,
            log_step_count_steps=log_count_steps,
            scaffold=scaffold,
            hooks=[log_hook, stop_iter_hook, max_runtime_hook,
                   save_at_end_hook]) \
            as sess:

        if ckpt_file is not None:
            print('Loading checkpoint files {}'.format(ckpt_file))
            saver.restore(sess, ckpt_file)
            saver_start_step.restore(sess, ckpt_file)

        print('Starting training.', flush=True)

        while not sess.should_stop():
            sess.run(train_step)

        print('Finished training.', flush=True)
